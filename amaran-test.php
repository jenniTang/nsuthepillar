<!DOCTYPE html>
<html>
<head>
    <title>My Web Page</title>
    <link rel="stylesheet" href="dist/css/amaran.min.css">
    <link rel="stylesheet" href="dist/css/animate.min.css">
    <link href="font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <p>My Content</p>
    <button id="start">start</button>
</body>
<script src="js/jquery.min.js"></script>
<script src="dist/js/jquery.amaran.js"></script>
    <script>
	    $(function(){
	    	$('#start').on('click',function(){
				$.amaran({
                    'message'   :'Your message has been sent.',
                    'position'  :'bottom right',
                    'inEffect'  :'slideBottom'
                });
			});
	    });
    </script>
</html>