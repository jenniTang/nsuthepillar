<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

<!-- SPORTS -->
	<section id="ccr-latest-post-gallery">
		<div class="ccr-gallery-ttile" id="sports">
			<span></span> 
			<p class="w3-animate-fading">SPORTS EDITORIAL</p>
		</div><!-- .ccr-gallery-ttile -->

	<!-- When Ring and Podium Collide -->
		<div class="row w3-animate-opacity">
			<div class="col-md-12"><br>
				<h3 class="text-center"><strong>When Ring and Podium Collide</strong></h3>
				<h6 class="text-center"><i>by Rommel V. Cayon</i></h6>

				<p class="justify"><span class="margin-3"></span>Emmanuel “Manny” Pacquiao is renowned as one of the greatest boxer in the world. Many were inspired by his passion and commitment to the boxing world. But now, he looks torn between the two things that made him what he is now. Being a boxer and and law-maker.</p>
				<p class="justify"><span class="margin-3"></span>Hauling twelve championship title and brought pride and honor in the Philippines. But behind of greatness and being top in the world of boxing, there were remonstrance that questioned his career in boxing and especially as lawmaker. Is he more deserving to be in the senate or just stay in the boxing world?</p>
				<p class="justify"><span class="margin-3"></span>Before Pacquiao was elected as one of the senators and served his Province as Congressman in the House of Representatives in two terms, Pacquiao wanted to hang up his gloves to focus on his political career, but promises are meant to be broken. </p>
				<p class="justify"><span class="margin-3"></span>Pacquiao continues what really his passion and come back again to the boxing ring. Even his top priority was his political career but his love for boxing had not diminished. After this statement from Pacquiao, there were a lot of opinions criticizing his duties as a lawmaker.</p>
				<p class="justify"><span class="margin-3"></span>Pacquiao urged to focus and concentrate on his position as lawmaker after losing against Jeff Horn of Australia last year, but despite this, Pacquiao would continue to make his country proud. People can’t deny the fact that Pacquiao is a legend in the world of boxing, but how can he works as senator if he commits absences in the senate and allotted his time for his training for his fight? Is he really still an effective lawmaker despite of those absences in the senate?</p>
				<p class="justify"><span class="margin-3"></span>After all the challenges that Pacquiao faced on his boxing career and as being a lawmaker, he brought again pride and honor in the Philippines as he drubbed Lucas Matthysse of Mexico in WBA Welterweight Championship held at Kuala Lumpur, Malaysia last July 15. At the age of 39, Pacquiao declared that he still had the passion for the sport, a passion that brought a change and big impact to his life. </p>
			</div>
		</div>

		<section class="bottom-border2">
		</section> <!-- /#bottom-border -->

	</section>

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php';?>