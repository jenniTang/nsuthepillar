<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="" >
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>NSU | The Pillar</title>
	<link rel="icon" href="img/pic_release/pillar_logo2.png">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/w3.css">
	<link rel="stylesheet" type="text/css" href="w32.css">
	<script type="text/javascript" src="w32.js"></script>
	<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
	<![endif]-->

</head>
<style>
.read-more input:hover{
	background-color: rgba(0,0,0,0.05);
	color: rgba(128,128,128,0.5);
	-webkit-transition: all .15s ease-in;
	   -moz-transition: all .15s ease-in;
	    -ms-transition: all .15s ease-in;
	     -o-transition: all .15s ease-in;
	        transition: all .15s ease-in;
}
.read-more input{
	color: rgba(0,0,0,0.9);
	background-color: rgba(0,0,0,0.075);
	display: inline-block;
	padding: 0px 10px;
	line-height: 30px;
	border: 1px dashed rgba(0,0,0,0.5);
}
.contact-us-title{
	color: #fff;
}
.contact-us-title p{
	color: #fff;
}
.contact-us{
	color: #fff;
	background-color: rgba(0,0,0,0.075);
	display: inline-block;
	padding: 0px 10px;
	line-height: 30px;
	border: 1px dashed rgba(0,0,0,0.9);
}
.contact-us2{
	color: #fff;
	background-color: rgba(0,0,0,0.075);
	display: inline-block;
	padding: 0px 10px;
	line-height: 30px;
}
.contact-us{
	color: #fff;
	background-color: rgba(0,0,0,0.075);
	display: inline-block;
	padding: 0px 10px;
	line-height: 30px;
	border: 1px dashed rgba(0,0,0,0.9);
}
.bottom-border2{
	width: 100%;
	padding-top: 20px;
	margin-bottom: 10px;
	border-bottom: 1px solid #ccc;
}

.btn:hover{
	background-color: #e6e6fe !important;
	color: rgba(0,0,0,0.9);
	-webkit-transition: all .15s ease-in;
	   -moz-transition: all .15s ease-in;
	    -ms-transition: all .15s ease-in;
	     -o-transition: all .15s ease-in;
	        transition: all .15s ease-in;	
}

.pre-title{
	background-color: rgba(182, 181, 96, 0.05);
	color: rgba(0,0,0,0.4) !important;
}
.margin-3{
	margin-left: 3em;
}
.margin-1{
	margin-left: 1em;
}
.justify{
	text-align: justify;
	margin-top: 10px;
}
.ccr-footer-sidebar{
	background-color: #282c2d;
	background-image: url(img/footer-sidebar-bg.png);
	width: 100%;
}
#info-body{
    background-color: #f0f8ff !important;
    border: 1px solid rgba(182, 181, 96, 0.30); 
    margin-top: 1em;
    border-radius: 0px 0px 5px 5px;
    box-shadow: inset 0 1px 10px 3px rgba(182, 181, 96, 0);
}
</style>
<script>

	function printPage() {
	    window.print();
	}

	function startTime() {
		var today = new Date(Date.now());
		var h = today.getHours();
		var m = today.getMinutes();
		var s = today.getSeconds();
		m = checkTime(m);
		s = checkTime(s);
		document.getElementById("txt").innerHTML= h+":"+m+":"+s;
		var t = setTimeout(startTime, 500);
	}
	function checkTime(i){
		if(i<10){i = "0" + i};
		return i;
	}

	var myVar = setInterval(myTimer, 1000);
	function myTimer() {
	    var d = new Date();
	    document.getElementById("localTime").innerHTML = d.toLocaleTimeString();
	}
</script>
<body onload="startTime()" style="background-color: #e6e6fe;">


<header id="ccr-header">
	
	<section id="ccr-site-title" style="background-color: rgba(74,73,143,75); background-image: url(img/footer-sidebar-bg.png);">
		<div class="container">
			<div class="site-logo">
				<a href="index">
					<img src="img/pic_release/pillar_logo.png" alt="Side Logo"/>
						<h1 class="w3-animate-right">THE <span>PILLAR</span></h1>
						<h6 class="w3-animate-right">Official Student Publication of NSU</h6>
				</a>
			</div> <!-- / .navbar-header -->
		</div>	<!-- /.container -->
	</section> <!-- / #ccr-site-title -->



	<section id="ccr-nav-main">
		<nav class="main-menu">
			<div class="container" >
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".ccr-nav-main">
			            <i class="fa fa-bars"></i>
		          	</button> <!-- /.navbar-toggle -->
				</div> <!-- / .navbar-header -->
				<div class="collapse navbar-collapse ccr-nav-main text-center">
					<ul class="nav navbar-nav">
						<!--  id="ccr-nav-main" class="active" -->
						<li><a href="index#news">NEWS</a></li>
						<li><a href="op-ed#op-ed">OP-ED</a></li>
						<li><a href="feature#feature">FEATURE</a></li>
						<!-- <li><a href="#">COLUMNS</a></li> -->
						<li><a href="sports#sports">SPORTS</a></li>
						<li><a href="about-us#about-us">ABOUT US</a></li>
						<li><a href="#contactUs">CONTACT US</a></li>
					</ul> <!-- /  .nav -->
				</div><!-- /  .collapse .navbar-collapse  -->

				<!-- <div id="currentTime">
					<h5 class="pull-right" id="localTime"></h5><h5 class="pull-right"><?=date('[D] M d, Y');?><i class="fa fa-clock-o margin-1"></i></h5>
				</div> -->
			</div>	<!-- /.container -->
		</nav> <!-- /.main-menu -->
		<div id="currentTime">
			<h5 class="pull-right current-time" id="localTime" style="margin-top: -1px;"></h5><h5 style="margin-top: -1px;" class="pull-right current-time"><?=date('[D] M d, Y');?><i class="fa fa-clock-o margin-1"></i></h5>
		</div>
	</section> <!-- / #ccr-nav-main -->
	
</header>

<section id="ccr-main-section news"><br>
	<div class="container">
		<aside id="ccr-right-section" class="col-md-4 col-sm-6">
			<section id="ccr-find-on-fb">
				<div class="find-fb-title">
					<span><i class="fa fa-facebook"></i></span> Find us on Facebook
				</div> <!-- /.find-fb-title -->
				<div class="find-on-fb-body">
					<a target="_blank" href="http://www.facebook.com/nsuthepillarpub/">http://www.facebook.com/nsuthepillarpub/</a>
					<p style="margin-top: 10px;">You can also visit:</p>
					<a target="_blank" href="https://www.facebook.com/NSUisYOU/">https://www.facebook.com/NSUisYOU/</a>
					<div class="fb-like-box" data-href="#" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
				</div> <!-- /.find-on-fb-body -->
				<br>
			</section> <!-- /#ccr-find-on-fb -->

			<section id="ccr-sidebar-add-place">
				<div>
					<img src="img/pic_release/new-cover.jpg">
				</div>
				<br>
			</section> <!-- /#ccr-sidebar-add-place -->


		</aside><!-- /.col-md-8 / #ccr-left-section -->


			<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">
				<div class="col-md-12" id="about-us">
					<h1>The Pillar Publication</h1>
					<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
					</section> <!-- /#bottom-border -->
					<p class="justify"><span class="margin-3"></span>The Pillar Publication, the official tertiary publication of NSU, is where campus journalists belong. Driven with the innate goal of promoting the welfare of the students and the just dissemination of information, the publication has continually served the very purpose of its existence.</p>
					<p class="justify"><span class="margin-3"></span>Pillar is defined as a large post that helps to hold up something. Philosophically, the publication, for years, has held an important role on upholding the legacy carved into the history page of NSU.</p>
					<p class="justify"><span class="margin-3"></span>On its 29 years of service since its establishment on the year 1988 as one of the leading student organizations in the campus, The Pillar has evidently contributed service to the institution and to the studentry. It had even transcended its role from a mere publication into a prime mover of change as it advocates students’ empowerment, bringing the NSU to higher grounds.</p>
					<p class="justify"><span class="margin-3"></span>Proving that the publication is a powerhouse of competitive and skilled student-journalists, The Pillar has given honor to the university by winning different regional competitions like the Campus Press of the Year Award on the Regional Tertiary Press Conference (RTSPC) last 2013, and bagging major awards on various national competitions.</p>
				</div>

				<section class="bottom-border2">
				</section> <!-- /#bottom-border -->

				<section class="bottom-border">
				</section> <!-- /#bottom-border -->
			</section>


<?php include'footer.php';?>