<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

<!-- FEATURE -->
	<section id="ccr-latest-post-gallery">
		<div class="ccr-gallery-ttile" id="feature">
			<span></span> 
			<p class="w3-animate-fading">SPORTS EDITORIAL</p>
		</div><!-- .ccr-gallery-ttile -->

	<!-- FIND YOUR FIT -->
		<div class="row w3-animate-opacity">
			<div class="col-md-12"><br>
				<h4 class="text-center"><strong>FIND YOUR FIT</strong></h4>
				<h6 class="text-center"><i>By Joshua Gibson Fuentes</i></h6>
				<p class="justify">Empowering students to join organizations and participate on its activities has been one of the highlights of the NSU’s banner program dubbed as ‘NSU is YOU’.</p>
				<p class="justify"><span class="margin-3"></span>As soon as the school year kicked off last June, different student organizations has started setting off their drives and own way of adverting students’ attention and recruiting some prospect members. </p>
				<p class="justify"><span class="margin-3"></span>But hey, out of the more than 30 registered student organizations in the university, have you already made up your mind to which org you want to be part of? I bet you still have not. Well, let me give you some sort-of-info of some of the major orgs in the campus. </p>
				<p class="justify"><span class="margin-3"></span>So c’mon, read this, be acquainted of these clubs and together, LET’S FIND YOUR FIT!</p>
				<br><br>
					<div class="read-more pull-right">
						<a href="report/find-your-fit#read">Read More</a>
					</div><br>
			<section class="bottom-border2">
			</section> <!-- /#bottom-border -->
			</div>
		</div>

	<!-- A Pride Affair: -->
		<div class="row w3-animate-opacity">
			<div class="col-md-12"><br>
				<h6 class="text-center">A Pride Affair:</h6>
				<h4 class="text-center"><strong>The Love You Never Knew</strong></h4><br>
				<p class="justify"><span class="margin-3"></span>Since the past millennia even since before Christ, the common notion that a woman was made for a man has been an inviolable norm that people has strongly believed. It has then set a vital convention that a person coexists to live with a person of his opposite sex. Yet, as years went on, third sex, the homosexual men and women who are romantically or sexually attracted to other men and women, suddenly came into being- the gay and lesbian.</p>
				<p class="justify"><span class="margin-3"></span>As of this modern age, the conception that love knows no sexuality has predominated that it has defied the perception that coupling two individuals with opposite sexuality is a revered custom when men became attracted to men and women to another women. But what if Cupid fools around and shoot its love arrows to a gay and a lesbian? Here is an atypical love story.</p>
				<br><br>
					<div class="read-more pull-right">
						<a href="report/a-pride-affair#read">Read More</a>
					</div><br>
			<section class="bottom-border2">
			</section> <!-- /#bottom-border -->
			</div>
		</div>

	</section>

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php';?>