<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">
	<div class="ccr-last-update">
		<div class="update-ribon">Last Update</div> <!-- /.update-ribon -->
		<span class="update-ribon-right"></span> <!-- /.update-ribon-left -->
		<div class="update-news-text" id="update-news-text">
				<ul id="latestUpdate">  
				    <li class="w3-animate-fading"><a href="#">During the Founding Aniversary</a></li>
				</ul>
		</div> <!-- /.update-text -->

		<div class="update-right-border"></div> <!-- /.update-right-border -->
	</div> <!-- / .ccr-last-update -->

	<section id="ccr-slide-main" class="carousel slide" data-ride="carousel">				
		<!-- Carousel items -->
		<div class="carousel-inner">
			<div class="active item">
				<div class="container slide-element">
					<img src="img/pic_release/parade2.png" alt="Main Slide Image 1">
				</div> <!-- /.slide-element -->
			</div> <!--/.active /.item -->
			<div class="item">
				<div class="container slide-element">
					<img src="img/pic_release/chorale2.png" alt="Main Slide Image 1">
				</div> <!-- /.slide-element -->
			</div> <!--  /.item -->
			<div class="item">
				<div class="container slide-element">
					<img src="img/pic_release/1027.png" alt="Main Slide Image 1">
				</div> <!-- /.slide-element -->
			</div> <!-- .item -->
			<div class="item">
				<div class="container slide-element">
					<img src="img/pic_release/0477.png" alt="Main Slide Image 1">
				</div> <!-- /.slide-element -->
			</div> <!-- /.item -->
			<div class="item">
				<div class="container slide-element">
					<img src="img/pic_release/0066.png" alt="Main Slide Image 1">
				</div> <!-- /.slide-element -->
			</div> <!-- /.item -->
			<div class="item">
				<div class="container slide-element">
					<img src="img/pic_release/1311.png" alt="Main Slide Image 1">
				</div> <!-- /.slide-element -->
			</div> <!-- /.item -->				
		</div> <!-- /.carousel-inner -->
		<!-- slider nav -->
		<a class="carousel-control left" href="#ccr-slide-main" data-slide="prev"><i class="fa fa-arrow-left"></i></a>
		<a class="carousel-control right" href="#ccr-slide-main" data-slide="next"><i class="fa fa-arrow-right"></i></a>
		<ol class="carousel-indicators">
			<li data-target="#ccr-slide-main" data-slide-to="0" class="active"></li>
			<li data-target="#ccr-slide-main" data-slide-to="1"></li>
			<li data-target="#ccr-slide-main" data-slide-to="2"></li>
			<li data-target="#ccr-slide-main" data-slide-to="3"></li>
			<li data-target="#ccr-slide-main" data-slide-to="4"></li>
			<li data-target="#ccr-slide-main" data-slide-to="5"></li>
		</ol> <!-- /.carousel-indicators -->			
	</section><!-- /#ccr-slide-main -->

<!-- NEWS -->
	<section id="ccr-latest-post-gallery">
		<div class="ccr-gallery-ttile" id="news">
			<span></span> 
			<p class="w3-animate-fading">News</p>
		</div><!-- .ccr-gallery-ttile -->

		<!-- XXX Enrollment up by 6%;<br>CAS remains on top -->
			<div class="row w3-animate-opacity">
				<div class="col-md-6">
					<h4><strong>Enrollment up by 6%;<br>CAS remains on top</strong></h4>
					<h6><i>by Joy Castillo and Sherwin Sarzuelo</i></h6>

					<p class="justify"><span class="margin-3"></span>Due to the K + 12 first batch graduates, Naval State University enrolment rate increased by 6% with a total population of 7,637 as the first semester, School Year 2018-2019 commenced last June 25 with College of Arts and Sciences (CAS) taking the highest number of enrollees.</p><br><br>
					<div class="read-more pull-right">
						<a href="report/enrollment-up-by-sixpercent-cas-remains-on-top#read">Read More</a>
					</div>	
				</div>
				<br><br>
				<div class="col-md-6">
					<img src="img/pic_release/visual.png"><br><br>
				</div>	
			</div>
			<section class="bottom-border2">
			</section> <!-- /#bottom-border -->


			<div class="row w3-animate-opacity">
		<!-- XXX Univ takes part in Nat’l Simultaneous Quake Drill -->
				<div class="col-md-6">
					<h4><strong>Univ takes part in Nat’l Simultaneous Quake Drill</strong></h4>
					<h6><i>by Tonette Grace Orbena</i></h6>
					<p class="justify"><span class="margin-3"></span>With the hashtag “#BidaAngHanda,” members of Municipal Disaster Risk Reduction Management Council (MDRRMC): Philippines National Police (PNP), Bureau of Fire Protection (BFP), Armed Forces of the Philippines (AFP), Provincial Disaster Risk Reduction Management Office (PDRRMO), Naval Rescue Unit (NAVRU) . . . <!-- and Philippine Coast Guard Auxiliary (PCGA) took part in the drill. Instructors, students, experts and volunteers of the university, which include Army ROTC and Nursing students, also jumped on action. --></p>
					<br><br>
					<div class="read-more pull-right">
						<a href="report/univ-takes-part-in-nat’l-simultaneous-quake-drill#read">Read More</a>
					</div>
					<br>
					<section class="bottom-border2">
					</section> <!-- /#bottom-border -->
				</div>
		<!-- XXX NSU hosts EVCIEERD Strategic Planning 2018 -->
				<div class="col-md-6">
					<h4><strong>NSU hosts EVCIEERD Strategic Planning 2018</strong></h4>
					<h6><i>by Rogelio D. Dimakiling Jr.</i></h6>
					<p class="justify"><span class="margin-3"></span>Ready for this year’s improvements and innovations in the Research and Development (R&amp;D) sector, Naval State University (NSU) hosted the 2018 Strategic Planning initiated by the Eastern Visayas Consortium for Industry, Energy, and Emerging Technology Research and Development (EVCIEERD) last July 30 at the University Hostel.</p>
					<br><br>
					<div class="read-more pull-right">
						<a href="report/nsu-hosts-evcieerd-strategic-planning-twentyeighteen#read">Read More</a>
					</div>
					<br>
					<section class="bottom-border2">
					</section> <!-- /#bottom-border -->
				</div>
			</div>

			<div class="row w3-animate-opacity">
		<!-- XXX NSU to vie for Nat’l Chorale competition -->
				<div class="col-md-6">
					<h4><strong>NSU to vie for Nat’l Chorale competition</strong></h4>
					<h6><i>by Bebeneth Garcia</i></h6>
					<p class="justify"><span class="margin-3"></span>Proving that NSU does not only excel in the field of instruction but also a powerhouse of talents, Naval State University Faculty Chorale, the only entity from Eastern Visayas, will compete for the 2018 Government Chorale Competition at the Cultural Center of the Philippines (CCP) . . . <!-- this coming September 18. --></p>
					<br><br>
					<div class="read-more pull-right">
						<a href="report/nsu-to-vie-for-nat’l-chorale-competition#read">Read More</a>
					</div>
					<br>
					<section class="bottom-border2">
					</section> <!-- /#bottom-border -->
				</div>
		<!-- XXX 11 programs submitted for COPC -->
				<div class="col-md-6">
					<h4><strong>11 programs submitted for COPC</strong></h4>
					<h6><i>by Joy Castillo and Tonette Grace Orbena</i></h6>
					<p class="justify"><span class="margin-3"></span>As a requirement for on-job-trainings or internship and a mandatory requirement of the Commission on Higher Education (CHED), programs from different colleges of Naval State University submitted for Certificate of Program Compliance (COPC) with the College of Education (COED) as the first to receive the certification last October 10, 2016.</p>
					<br><br>
					<div class="read-more pull-right">
						<a href="report/eleven-programs-submitted-for-copc#read">Read More</a>
					</div>
					<br>
					<section class="bottom-border2">
					</section> <!-- /#bottom-border -->
				</div>
			</div>

			<div class="row w3-animate-opacity">
		<!-- XXX Studs join Wet ‘n Wild Acquaintance Party -->
				<div class="col-md-6">
					<h4><strong>Studs join Wet ‘n Wild Acquaintance Party</strong></h4>
					<h6><i>by Abegail Mondelo and Joevenil Jamin</i></h6>
					<p class="justify"><span class="margin-3"></span>NSU grounds turned into a wild space as Naval State University once again celebrated its Annual Acquaintance Party with the “Paint Party” theme that turned into “Wet and Wild” as it rained last July 11 at the NSU Oval Grounds.</p>
					<br><br>
					<div class="read-more pull-right">
						<a href="report/studs-join-wet ‘n-wild-acquaintance-party#read">Read More</a>
					</div>
					<br>
					<section class="bottom-border2">
					</section> <!-- /#bottom-border -->
				</div>
		<!-- XXX Nutrition Month 2018 seals off;<br>CAS dominates -->
				<div class="col-md-6">
					<h4><strong>Nutrition Month 2018 seals off;<br>CAS dominates</strong></h4>
					<p class="justify"><span class="margin-3"></span>Deafening cheers, pounding beat of music and crowded scene seal off the entire Naval State University Hostel during this year’s 44th Nutrition Month Celebration anchored with the theme “Ugaliing Magtanim, Sapat na Nutrisyon Aanihin,” hosted by the Health and Sciences Department last July 25.</p>
					<br><br>
					<div class="read-more pull-right">
						<a href="report/nutrition-month-twentyeighteen-seals-off-cas-dominates#read">Read More</a>
					</div>
					<br>
					<section class="bottom-border2">
					</section> <!-- /#bottom-border -->
				</div>
			</div>

			<div class="row w3-animate-opacity">
		<!-- XXX BSBA stud represents Phil in ASEAN Youth Camp -->
				<div class="col-md-6">
					<h4><strong>BSBA stud represents Phil in ASEAN Youth Camp</strong></h4>
					<h6><i>by Frallyn Candido and Melanie Betcher</i></h6>
					<p class="justify"><span class="margin-3"></span>As one of the five representatives of the Philippines, Ralph Christian J. Martinez, BSBA student attended the ASEAN Youth Camp 2018 representing the Naval State University at Bangkok, Thailand, April 24-30.</p>
					<br><br>
					<div class="read-more pull-right">
						<a href="report/bsba-stud-represents-phil-in-asean-youth-camp#read">Read More</a>
					</div>
					<br>
					<section class="bottom-border2">
					</section> <!-- /#bottom-border -->
				</div>
		<!-- XXX NSU’s infra projects boom -->
				<div class="col-md-6">
					<h4><strong>NSU’s infra projects boom</strong></h4>
					<h6><i>by Julita Abrigo and Melanie Betcher</i></h6>
					<p class="justify"><span class="margin-3"></span>As part of new administration’s strategic direction, NSU isYOU, specifically on the Modernization of Facilities, Naval State University, through the Engineering and General Services Office (EGSO) simultaneously launched a number of infrastructure projects . . . <!-- for the School Year 2017-2018 and 2018-2019 amounted up to 312M. --></p>
					<br><br>
					<div class="read-more pull-right">
						<a href="report/nsu’s-infra-projects-boom#read">Read More</a>
					</div>
					<br>
					<section class="bottom-border2">
					</section> <!-- /#bottom-border -->
				</div>
			</div>

			<div class="row w3-animate-opacity">
		<!-- XXX NSU inks MOA with Spanish youth org -->
				<div class="col-md-6">
					<h4><strong>NSU inks MOA with Spanish youth org</strong></h4>
					<h6><i>by Rommel Cayon</i></h6>
					<p class="justify"><span class="margin-3"></span>To forge partnership with the Mission Cebu of Madrid, Spain (MCMS), a group of Spanish youth missionaries, Naval State University signed a Memorandum of Agreement (MOA) during the welcome program hosted . . . <!-- for the organization held at the NSU Hostel, July 20. --></p>
					<br><br>
					<div class="read-more pull-right">
						<a href="report/nsu-inks-moa-with-spanish-youth-org#read">Read More</a>
					</div>
					<br>
					<section class="bottom-border2">
					</section> <!-- /#bottom-border -->
				</div>
		<!-- XXX FSC pushes stud orgs incentives -->		
				<div class="col-md-6">
					<h4><strong>FSC pushes stud orgs incentives</strong></h4>
					<h6><i>by Joshua Gibson Fuentes</i></h6>
					<p class="justify"><span class="margin-3"></span>To substitute the vacated scholarships with the implementation of the Republic Act 10931 otherwise known as Universal Free Higher Education Act, Federation of Student Council (FSC) proposed for a monetary incentive for members of student organizations in the university.</p>
					<br><br>
					<div class="read-more pull-right">
						<a href="report/fsc-pushes-stud-orgs-incentives#read">Read More</a>
					</div>
					<br>
					<section class="bottom-border2">
					</section> <!-- /#bottom-border -->
				</div>
			</div>

			<div class="row w3-animate-opacity">
		<!-- XXX Nursing dep’t lauds 100% NLE passers -->
				<div class="col-md-6">
					<h4><strong>Nursing dep’t lauds 100% NLE passers</strong></h4>
					<h6><i>by Allan Delgado and Jundel Mallen</i></h6>
					<p class="justify"><span class="margin-3"></span>Consistent to its record of producing commendable passing percentages for the Nursing Licensure Examination (NLE) since its establishment, the Nursing Department once again takes pride as it produced new set of nurses with 100% passing percentage last June 3-4.</p>
					<br><br>
					<div class="read-more pull-right">
						<a href="report/nursing-dep’t-lauds-onehundredpercent-nle-passers#read">Read More</a>
					</div>
					<br>
					<section class="bottom-border2">
					</section> <!-- /#bottom-border -->
				</div>
		<!-- XXX NSU hosts Sustainable Energy Forum -->
				<div class="col-md-6">
					<h4><strong>NSU hosts Sustainable Energy Forum</strong></h4>
					<h6><i>by Ronalyn Ribot</i></h6>
					<p class="justify"><span class="margin-3"></span>To bring knowledge and movement towards energy conservation, the Action for Economic Reforms and Friedrich Elbert Stiftung brings a sustainable energy forum entitled “Renewable and Sustainable Energy for Industrial Development: Engaging the Transition Towards a Greener . . . <!-- and Resilient Future” at Naval State University on July 24. --></p>
					<br><br>
					<div class="read-more pull-right">
						<a href="report/nsu-hosts-sustainable-energy-forum#read">Read More</a>
					</div>
					<br>
					<section class="bottom-border2">
					</section> <!-- /#bottom-border -->
				</div>
			</div>

			<div class="row w3-animate-opacity">
				<div class="col-md-6"></div>
		<!-- XXX NSU ULRC Gearing Towards Modernization -->
				<div class="col-md-6">
					<h4><strong>NSU ULRC Gearing Towards Modernization</strong></h4>
					<h6><i>by Deal Joseph Morillo</i></h6>
					<p class="justify"><span class="margin-3"></span>The NSU University Learning Resource Center is taking actions for the modernization of its facilities as well as the expansion of the building to better serve the students and the staff of NSU.</p>
					<br><br>
					<div class="read-more pull-right">
						<a href="report/nsu-urlc-gearing-towards-modernization#read">Read More</a>
					</div>
					<br>
					<section class="bottom-border2">
					</section> <!-- /#bottom-border -->
				</div>
			</div>

	</section> <!--  /#ccr-latest-post-gallery  -->
				 
	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section><!-- / .col-md-4  / #ccr-right-section -->

<?php include'footer.php';?>