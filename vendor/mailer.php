<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require '../vendor/autoload.php';

$mail = new PHPMailer(true); 

try {
    //Server settings
    $mail->SMTPDebug = 2;
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com'; 
    $mail->SMTPAuth = true;
    $mail->Username = 'nsuthepillarpublication@gmail.com';
    $mail->Password = 'publication2018';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->setFrom($_POST['email'], 'Pillar Friend');
    $mail->addAddress('nsuthepillarpublication@gmail.com' , 'Recipient1');
    $mail->addReplyTo($_POST['email'], 'Pillar Friend');
    
    //Content
    $mail->isHTML(true); 
    $mail->Subject = 'Site Feedback';
    $mail->Body    = 'Hi Im '.$_POST['name'].'! '.$_POST['msg'];

    $mail->send();
    $echo = 'Hey '.$_POST['name'].'! Your message has been sent.';
    header('Location: ../?'.$echo.'&s=1110101011101#contactUs');

} catch (Exception $e) {
    $echo = 'Sorry '.$_POST['name'].'Your message could not be sent.';
    $echo2 =  'Mailer Error: ' . $mail->ErrorInfo;
    header('Location: ../?'.$echo.$echo2.'&s=0110101011101#contactUs');
}