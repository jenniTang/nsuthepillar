<?php include'header.php'; ?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">
	<div class="ccr-last-update" id="op-ed">
		<div class="update-ribon"></div> <!-- /.update-ribon -->
		<span class="update-ribon-right"></span> <!-- /.update-ribon-left -->
		<div class="update-news-text" id="update-news-text">
				<ul id="latestUpdate">  
				    <li class="w3-animate-fading"><a href="#">Cartoon by Aljon</a></li>
				</ul>
		</div> <!-- /.update-text -->

		<div class="update-right-border"></div> <!-- /.update-right-border -->
	</div> <!-- / .ccr-last-update -->

		<div class="row w3-animate-zoom">
			<div class="col-md-3"></div>
			<div class="col-md-6 featured-world-news-post">
				<img src="img/pic_release/cartoon-by-aljon.png">
			</div>
		</div>

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

<!-- OP-ED -->
	<section id="ccr-latest-post-gallery">
		<div class="ccr-gallery-ttile">
			<span></span> 
			<p class="w3-animate-fading">EDITORIALS OPINION</p>
		</div><!-- .ccr-gallery-ttile -->
	
		<div class="row w3-animate-opacity">
	<!-- XXX VICtorious NSU -->
			<div class="col-md-6">
				<h4><strong>VICtorious NSU</strong></h4>
				<p class="justify"><span class="margin-3"></span><strong>In its 9th year of being proclaimed as a State University, NSU continues its culture of excellence as mirrored in its recent success in obtaining an ISO 9001: 2015 certification. </strong></p>
				<p class="justify"><span class="margin-3"></span>ISO, acronym for International Organization for Standardization, is an International Association for Accrediting State Universities and Colleges. The ISO 9001 quality management system, on the other hand, is a systematic and process driven . . . <!-- approach designed to support a company in meeting the needs of its customers. --></p>
				<br><br>
					<div class="read-more pull-right">
						<a href="report/victorious-nsu#read">Read More</a>
					</div><br>
				<section class="bottom-border2">
				</section> <!-- /#bottom-border -->
			</div>
	<!-- XXX Energy Madness -->
			<div class="col-md-6">
				<h4><strong>Energy Madness</strong></h4>
				<h6><i>By Rogelio Dimakiling, Jr.</i></h6>
				<p class="justify"><span class="margin-3"></span>A clear solution or a firm objection, Almeria takes blows as bets on its geothermal resources rages until the final verdict.</p>
				<p class="justify"><span class="margin-3"></span>The island has never been this fired up as issues and possible answers that wish to address energy crisis in the province as well as Region VIII continue to elevate between the local government and BGI.</p>
				<br><br>
					<div class="read-more pull-right">
						<a href="report/energy-madness#read">Read More</a>
					</div><br>
				<section class="bottom-border2">
				</section> <!-- /#bottom-border -->
			</div>
		</div>

		<div class="row w3-animate-opacity">
	<!-- XXX Change has Come -->
			<div class="col-md-6">
				<h4><strong>Change has Come</strong></h4>
				<h6><i>by Jade Banate</i></h6>
				<p class="justify"><span class="margin-3"></span>Tough. Blunt. Insensitive. Whenever I hear these three words, only one person comes to my mind: President Duterte.</p>
				<p class="justify"><span class="margin-3"></span>Two years ago, Duterte won the Presidential election and sat as the 16th President of the Philippine Republic. . . <!-- And just last July 23, he delivered his third State of the Nation Address (SONA). --></p>
				<br><br>
					<div class="read-more pull-right">
						<a href="report/change-has-come#read">Read More</a>
					</div><br>
				<section class="bottom-border2">
				</section> <!-- /#bottom-border -->
			</div>
	<!-- XXX Unjudicial Killings -->
			<div class="col-md-6">
				<h4><strong>Unjudicial Killings</strong></h4>
				<h6><i></i></h6>
				<p class="justify"><span class="margin-3"></span>The Philippines is in the middle of a war that is already lost even before it is even finished. A war that is waged with guns and bullets aimed to eradicate illegal drugs. But with the passage of time, various data clearly show that the poor and dispossessed are the main casualties of a war that was supposed to be indiscriminating in the first place.</p>
				<!-- <p class="justify"><span class="margin-3"></span>His Excellency, President Rodrigo Duterte sits at the driver seat on the notoriously bloody campaign against illegal drugs. Whatever his initial plan was, the aftermath has shown us the picture of a war that is merciless as it is futile. They may have killed thousands of illegal drug users and drug pushers, but they were not able to eradicate the problem from its root. It turns out that if you or your family members are powerful or hold a high government position, you are more likely to be exempted from being apprehended. </p> -->
				<br><br>
					<div class="read-more pull-right">
						<a href="report/unjudicial-killings#read">Read More</a>
					</div><br>
			<section class="bottom-border2">
			</section> <!-- /#bottom-border -->
			</div>
		</div>

	<!-- XXX Don’t be a Filipino! -->
		<div class="row w3-animate-opacity">
			<div class="col-md-6"></div>
			<div class="col-md-6">
				<h4><strong>Don’t be a Filipino!</strong></h4>
				<h6><i></i></h6>
				<p class="justify"><span class="margin-3"></span>If I were just a Filipino, I would condemn the directive of President Rodrigo Duterte to apprehend tambays like how most of the critics have been doing. However, it just so happened that I was born as a wise and practical Filipino. So I guess, I’ll just have to start teaching myself to go home earlier than 10:00 PM from now on.</p>
				<br><br>
					<div class="read-more pull-right">
						<a href="report/don’t-be-a-filipino!#read">Read More</a>
					</div><br>
			<section class="bottom-border2">
			</section> <!-- /#bottom-border -->
			</div>
		</div>

	</section>


<!-- OPINION -->
	<section id="ccr-latest-post-gallery">
		<div class="ccr-gallery-ttile" id="news">
			<span></span> 
			<p class="w3-animate-fading">OPINION</p>
		</div><!-- .ccr-gallery-ttile -->

	<!-- XXX Free WIFI: Now Loading -->
		<div class="row w3-animate-opacity">
			<div class="col-md-12">
				<h4><strong>Free WIFI: Now Loading</strong></h4>
				<h6><i>By Tonette Grace Orbena</i></h6>

				<p class="justify"><span class="margin-3"></span>“Ingon nay free wifi, wa pa lage? Last year pa man to...”</p>
				<p class="justify"><span class="margin-3"></span>Students of the university, especially from last year, all cry these same words. Last year, it was said that there will be free wifi for all of the university’s stakeholders. However, queries then followed when the realization of this project became unrealized. </p>
				<br><br>
					<div class="read-more pull-right">
						<a href="report/free-wifi-now-loading#read">Read More</a>
					</div><br>
			<section class="bottom-border2">
			</section> <!-- /#bottom-border -->
			</div>
		</div>

	</section>

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>