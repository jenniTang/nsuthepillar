<?php
include_once ('header.php');?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>Enrollment up by 6%;<br>CAS remains on top</strong></h3>
		<h6 class="text-center"><i>by Joy Castillo and Sherwin Sarzuelo</i></h6>

		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

			<p class="justify"><span class="margin-3"></span>Due to the K + 12 first batch graduates, Naval State University enrolment rate increased by 6% with a total population of 7,637 as the first semester, School Year 2018-2019 commenced last June 25 with College of Arts and Sciences (CAS) taking the highest number of enrollees.</p>
			<p class="justify"><span class="margin-3"></span>“For the past two years, our enrolment decreased because of the K to 12, but now since there are already graduates of Senior High School, that’s why it increased, also the graduates of batch 2015 or 2016 are still given the chance to enroll in college for them not to be left behind,” Marilyn Q. Ignacio, Acting University Registrar, said in an interview.</p>
			<p class="justify"><span class="margin-3"></span>First year students recorded a total of 2218, with 29% in overall population and data showed from S.Y. 2014-2015 with 7931 students and S.Y. 2015-2016 with 8496 students, upon the implementation of K+12 program under Republic Act 10533, NSU enrolment decreases with 7458 students for S.Y. 2016-2017 and 7207 students for S.Y. 2017-2018.</p>
			<p class="justify"><span class="margin-3"></span>“Aside from this, one of the reasons for the increase in enrolment is the implementation of the Free Education policy in the university which encourages students to continue their tertiary education. Even I, will continue my studies because education is free for all,” Mrs. Ignacio further stated.</p>
			<h6><strong>CAS remains on top</strong></h6>
			<p class="justify"><span class="margin-3"></span>Meanwhile, topping the chart for four years now since 2015, College of Arts and Sciences (CAS) recorded a total of 1,817 enrollees for the semester. Based on the data of the University Registrar as of July 2018, two of its programs, Bachelor of Business Administration (BSBA) and Bachelor of Criminology (BSCrim) ranked as the top programs with highest number of enrollees among all programs in the university with 745 and 684 students respectively.</p>
			<p class="justify"><span class="margin-3"></span>Having the highest number of enrollees, CAS aims for improving quality delivery of educational services by submitting its programs for the International Organization for Standardization (ISO) 9001 of 2015 certification, Accrediting Agency of Chartered Colleges and Universities of the Philippines (AACCUP) Audit and Certificate of Program Compliance (COPC).</p>

		<p class="justify"><span class="margin-3"></span></p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>