<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>Nursing dep’t lauds 100% NLE passers</strong></h3>
		<h6 class="text-center"><i>by Allan Delgado and Jundel Mallen</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>Consistent to its record of producing commendable passing percentages for the Nursing Licensure Examination (NLE) since its establishment, the Nursing Department once again takes pride as it produced new set of nurses with 100% passing percentage last June 3-4.</p>
		<p class="justify"><span class="margin-3"></span>“There were four takers, two first takers and also two re takers. For first takers we obtain 100% NLE passers and 50 % for re takers with an overall performance of 75 % last June 2018 examination,” Ms. Jovy Dia Saniel, nursing department chairperson, said.</p>
		<p class="justify"><span class="margin-3"></span>Moreover, passers were: Glendy Love S. Sanchez and Princess Chariness M. Tagpuno-first takers, and Alyssa Marie D. Vermug-retaker.</p>
		<h6><strong>Department’s Best Practices</strong></h6>
		<p class="justify"><span class="margin-3"></span>According to Ms. Saniel, department of Nursing practiced the approach of evaluating the students’ performance through ranking in every exam for them to be guided on what field they give much focus on and instructors will provide advices on what they are going to do.</p>
		<p class="justify"><span class="margin-3"></span>“Always show their ranking to encourage them to do better, to avoid discrimination, they all have screen names during their exams” she added.</p>
		<h6><strong>Passers while on review</strong></h6>
		<p class="justify"><span class="margin-3"></span>Glendy Love S. Sanchez, one of the NLE first taker passers, said that they weren’t able to have the exam together with their classmates because the November exam falls on Saturday and both of them were Seventh day Adventist.</p>
		<p class="justify"><span class="margin-3"></span>She also added the struggles she had encountered during the review, first is the time span of the review that instead of six months, it is only four months, second is physical instability (sleep deprived, we spent time more time on reading and writing notes) and being far away from my family since I have responsibilities left at home (as a mom and a wife).</p>
		<p class="justify"><span class="margin-3"></span>“My secret weapon in passing the NLE is 90% prayer and 10%read/Study. All those sleepless nights had been paid off with God’s grace and guidance. Though the exam was hard, but i’m sticking to God’s promises in Philippians 4:13 and Jeremiah 29:11,” Glendy Love S. Sanchez, one of the NLE first taker passers, said.</p>
		<p class="justify"><span class="margin-3"></span>She further explained that the faculty in their department never fails to motivate and prayed for them. They also told them to keep their common sense activated during the exam and Yes it worked since NLE is more on application and management in all health care settings.</p>
		<h6><strong>History</strong></h6>
		<p class="justify"><span class="margin-3"></span>Nursing and Health Sciences Department started as a ladderized program in 2002 through the leadership of Dr. Edita S. Genson with six-month live-in Caregiver course and two-year Associate Degree in Health Care under Board Resolution No. 28, Series of 2002 and which was eventually evolved into a two-year diploma in practical nursing to Bachelor of Science in Nursing through the passage of Board Resolution No. 40, series of 2006.</p>
		<p class="justify"><span class="margin-3"></span>Per approval of the NSU Board of regents, the department of nursing was created last 2007 and was extended further to offered a Four-year Bachelor of Science in Nursing Program after having granted with a certificate to operate from the Commission on Higher Education in July 2009.</p>
		<p class="justify"><span class="margin-3"></span>Pursuant to Executive Order no. 358 “to institutionalize a ladderized interface between Technical-Vocational Education and Training (TVET) and Higher Education (HE),” and in accordance to RA number 7722, otherwise known as the “Higher Education Act of 1994,” NSU has been granted with certificate of Authority Number 32 series of 2007 to operate the Bachelor of Science in Nursing Credit Transfer Program from the existing Caregiving and Health Care Services Programs registered under the Unified TVET Program Registration and Accreditation System (UTPRAS).</p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>