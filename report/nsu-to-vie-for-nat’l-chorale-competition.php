<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>NSU to vie for Nat’l Chorale competition</strong></h3>
		<h6 class="text-center"><i>by Bebeneth Garcia</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>Proving that NSU does not only excel in the field of instruction but also a powerhouse of talents, Naval State University Faculty Chorale, the only entity from Eastern Visayas, will compete for the 2018 Government Chorale Competition at the Cultural Center of the Philippines (CCP) this coming September 18.</p>
		<p class="justify"><span class="margin-3"></span>Chosen as one of the eight government agency qualifiers to vie in the National, the first-timer NSU Faculty Chorale with a total member of 30 teaching staff, was tasked to perform three Filipino Chorale Works: a song extolling one or more values promoted by the commission, a freely chosen a capella chorale work and the competition song “Kawani ng Gobyerno, Dekalidad ang Serbisyo.”</p>
		<p class="justify"><span class="margin-3"></span>“In the regional level, submission of entry was via Youtube, posted it and informed the National Civil Service Commission (NCSC) that we already uploaded the video; from there, they selected,” Mr. Ryan Teofel Arpon , NSU Cultural Office Director, stated.</p> 
		<p class="justify"><span class="margin-3"></span>The said competition is one of the major events of the Philippine Civil Service Anniversary (PCSA) held every September which aims to showcase musical creativity of talented government employees, promote Filipino culture and arts through chorale singing, and inculcate in the mind of public servants a culture of excellence in public service.</p>
		<p class="justify"><span class="margin-3"></span>“We conducted our first meeting last week and everyone is encouraged to have commitment,” Mrs. Reggie Nierra, Human Resource and Management Officer (HRMO), emphasized. “We had our practice last Sunday and will assure to have in the next following days,” she concluded.</p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>