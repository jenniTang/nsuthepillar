<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>BSBA stud represents Phil in ASEAN Youth Camp</strong></h3>
		<h6 class="text-center"><i>by Frallyn Candido and Melanie Betcher</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>As one of the five representatives of the Philippines, Ralph Christian J. Martinez, BSBA student attended the ASEAN Youth Camp 2018 representing the Naval State University at Bangkok, Thailand, April 24-30.</p>
		<p class="justify"><span class="margin-3"></span>“With regards to my experience, first, before that I’m glad to say that this opportunity would have not been possible without the support of the school. With my own experience, the government agency in Thailand was very welcoming,” Martinez said.</p>
		<p class="justify"><span class="margin-3"></span>Martinez, the president of the An Lantugi Debate Society, has been chosen as of the representatives after passing the screening of the Department of Environment and Natural Resources (DENR).</p>
		<p class="justify"><span class="margin-3"></span>“The DENR sent a letter telling to choose one student to represent the university. And in there, it was stated that must at least have the basic knowledge when it comes to English and is willing to learn,” he added.</p>
		<p class="justify"><span class="margin-3"></span>Furthermore, the privilege given after being awarded as one of the most sustainable and eco-friendly schools in the country made  NSU to be part of the ASEAN Youth Camp 2018.</p>
		<p class="justify"><span class="margin-3"></span>Themed “Youth Stepping Toward Environmental Sustainability,’’ the said convention aims to generate mutual understanding and promote learning on the sustainable development through the sufficiency economy as well as to see the best practices from the stakeholders in Thailand on how to align and implement Sustainable Development Goals (SDG) to the ASEAN youth representatives.</p>
		<p class="justify"><span class="margin-3"></span>“We learned a lot especially with what the Thailand government does when it comes to preserving nature. They’re trying to integrate business models with environmental friendly models,” Martinez retorted.</p>
		<p class="justify"><span class="margin-3"></span>Other delegates from the country includes students from Don Mariano Marcos State University, Western Philippines University, University of Santo Tomas and UP Diliman who were sponsored by the Thailand government and the DENR.</p>
		<p class="justify"><span class="margin-3"></span>“To the student of the Naval State University, I think we should really be mindful of our surroundings and should really care for our environment. Because as what I have learned, specifically in the fourth day, we went to elementary and high school and then the students there were very productive,” Martinez remarked.</p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>