<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>NSU hosts EVCIEERD Strategic Planning 2018</strong></h3>
		<h6 class="text-center"><i>by Rogelio D. Dimakiling Jr.</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>Ready for this year’s improvements and innovations in the Research and Development (R&amp;D) sector, Naval State University (NSU) hosted the 2018 Strategic Planning initiated by the Eastern Visayas Consortium for Industry, Energy, and Emerging Technology Research and Development (EVCIEERD) last July 30 at the University Hostel.</p>
		<p class="justify"><span class="margin-3"></span>“I hope and I pray that through our strategic planning, our teamwork continues as we work hand in hand for sustainable development and progress not only in our respective universities in our region but also to the entire Philippines,” Dr. Victor C. Cañezo, Jr., OIC University President, expressed in his welcome remarks.</p>
		<p class="justify"><span class="margin-3"></span>The 2-day event was attended by representatives of the different universities in the region, namely Eastern Visayas State University (EVSU), Leyte Normal University (LNU), Palompon Institute of Technology (PIT), Samar State University (SSU), Northwest Samar State University (NwSSU), Southern Leyte State University (SLSU), and NSU.</p>
		<p class="justify"><span class="margin-3"></span>Also, distinguished officials from the regional offices were present; namely Hon. Edgardo M. Esperancilla, Director of DOST Region VIII; Engr. Ramil T. Uy, DOST Consortium Coordinator; and Ms. Marivic Cuayzon, Chief of NEDA Region VIII Research and Development Division.</p>
		<p class="justify"><span class="margin-3"></span>“We are here to work on a strategic plan which is an organizational management activity. We want to set priorities. It’s time for us to talk it over and let’s try to look in our inner capacity and capability of our institution so that we can set the priorities selflessly for our own satisfaction and need of our institution but as a contribution to the consortium, efforts of orchestrating and having a dynamic intention that will address the minds and the lives of the energy and industry sector of the region,” Engr. Uy stated the overview of the activity.</p>
		<p class="justify"><span class="margin-3"></span>Furthermore,the outputs of the meeting will be presented to the SUC Presidents on August 3 at the Palompon Institute of Technology.</p>
		<p class="justify"><span class="margin-3"></span>With a quote from Hillary Clinton, “Failing to plan is planning to fail,” Dr. Cañezo stressed as EVCIEERD gears up for the greater good of the region. </p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>