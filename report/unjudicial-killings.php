<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>Unjudicial Killings</strong></h3>
		<h6 class="text-center"><i></i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>The Philippines is in the middle of a war that is already lost even before it is even finished. A war that is waged with guns and bullets aimed to eradicate illegal drugs. But with the passage of time, various data clearly show that the poor and dispossessed are the main casualties of a war that was supposed to be indiscriminating in the first place.</p>
		<p class="justify"><span class="margin-3"></span>His Excellency, President Rodrigo Duterte sits at the driver seat on the notoriously bloody campaign against illegal drugs. Whatever his initial plan was, the aftermath has shown us the picture of a war that is merciless as it is futile. They may have killed thousands of illegal drug users and drug pushers, but they were not able to eradicate the problem from its root. It turns out that if you or your family members are powerful or hold a high government position, you are more likely to be exempted from being apprehended.</p>
		<p class="justify"><span class="margin-3"></span>Last August of 2017, the University of Santo Tomas released an independent research on the profile of the slain alleged drug users and drug pushers. The data showed that the victims were mostly male, unemployed, and 90% were living in poverty. This information alone clearly summarizes the fact that the poor and powerless are the real victims of this crime while the mighty and powerful can go away with it using their money and influence. </p>
		<p class="justify"><span class="margin-3"></span>No matter how many times the administration would try to explain their mission to quell the high rate of illegal-drug crimes such as illegal drug possession, drug usage, drug selling and drug buying, the lives lost of those wrongly accused cannot be revived. We can only imagine the pain of the families survived by those who are wrongly accused of the crime and were not given a chance to change themselves.</p>
		<p class="justify"><span class="margin-3"></span>There were countless, innocent lives claimed by an unfair and arbitrary revolution. The war against drugs is monolithic at best and anti-poor at worst. It targets those who are helpless and unable to protect themselves against the powerful and influential. It wields its merciless machineries to the people who are marginalized in the society. The poor are the real victims while the powerful wash their hands clean without guilt or remorse.</p>
		<p class="justify"><span class="margin-3"></span>There is nothing much we can do to take back what had been lost. We are living witnesses that the justice system in our country chooses color, race and social status. But our fight to equality is still ongoing and we cannot lose our light for which others in the dark can see.</p>
	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>