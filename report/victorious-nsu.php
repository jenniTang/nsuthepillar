<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>VICtorious NSU</strong></h3>
		<h6 class="text-center"><i></i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span><strong>In its 9th year of being proclaimed as a State University, NSU continues its culture of excellence as mirrored in its recent success in obtaining an ISO 9001: 2015 certification. </strong></p>
		<p class="justify"><span class="margin-3"></span>ISO, acronym for International Organization for Standardization, is an International Association for Accrediting State Universities and Colleges. The ISO 9001 quality management system, on the other hand, is a systematic and process driven approach designed to support a company in meeting the needs of its customers.</p>
		<p class="justify"><span class="margin-3"></span>In order to obtain an ISO 9001: 2015 certification, NSU underwent series of evaluation in several fields. ISO accreditors assessed compliance on the following fields: the quality management system, document requirements management responsibility, resource management, product realization, and also measurement analysis and improvement. Months prior to the visit of the accreditation member, accreditation committee of the different colleges were hands-on in preparing the documents needed for certification.</p>
		<p class="justify"><span class="margin-3"></span>Boosted by the NSUisYOU banner program of the current administration, OIC University President Victor C. Cañezo Jr. leads the rest of the group to achieve holistic improvement of the university and the ISO 9001:2015 is a testimony of the school’s vision towards achieving academic excellence.</p>
		<p class="justify"><span class="margin-3"></span>With the ample amount of time for the preparation for the ISO accreditation, we are confident that getting the certification is not impossible to achieve. From the administration, to the faculty members, and to the staff of various departments and offices, the sense of unity is evident as NSU worked hand-in-hand for the hopes of achieving an ISO 9008: 2015 certification.</p>
		<p class="justify"><span class="margin-3"></span>It might be argued that NSU has more work to do in the improvement of its physical infrastructure and academic blueprint, but with the ISO 9001: 2015 certification, we can conclude that is this is only the beginning of even more success and achievements for Naval State University.</p>
		<p class="justify"><span class="margin-3"></span>In undertakings as tremendous and, at times, tedious as a program certification, it takes a village to accomplish the goal. With the ISO 9001: 2015 obtained by Naval State University, we are slowly but surely heading towards being a globally competitive state university.</p>
	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>