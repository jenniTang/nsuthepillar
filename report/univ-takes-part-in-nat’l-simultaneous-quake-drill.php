<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>Univ takes part in Nat’l Simultaneous Quake Drill</strong></h3>
		<h6 class="text-center"><i>by Tonette Grace Orbena</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>With the hashtag “#BidaAngHanda,” members of Municipal Disaster Risk Reduction Management Council (MDRRMC): Philippines National Police (PNP), Bureau of Fire Protection (BFP), Armed Forces of the Philippines (AFP), Provincial Disaster Risk Reduction Management Office (PDRRMO), Naval Rescue Unit (NAVRU) and Philippine Coast Guard Auxiliary (PCGA) took part in the drill. Instructors, students, experts and volunteers of the university, which include Army ROTC and Nursing students, also jumped on action.</p>
		<p class="justify">“It is one of the programs of Security and Safety Management in the Naval State University and it is also compliant to the NSED by the NDRRMC or OCD. Aside from that, it is also very timely because we have 7,000 more or less population of students that should prepare about the earthquake because that is the only [phenomena that] existed [ that is] unpredictable,” Captain Pablo Cepriano, Security and Safety Management Officer explained.</p>
		<p class="justify"><span class="margin-3"></span>In addition, Capt. Cepriano also said that, aside from the drill, his team is on the process of planning to conduct first aid and fire fighting training, make manual for contingency planning and emergency planning for hostage taking, bomb threat preparations and others.</p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>