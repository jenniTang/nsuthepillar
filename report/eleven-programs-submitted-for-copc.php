<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>11 programs submitted for COPC</strong></h3>
		<h6 class="text-center"><i>by Joy Castillo and Tonette Grace Orbena</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>As a requirement for on-job-trainings or internship and a mandatory requirement of the Commission on Higher Education (CHED), programs from different colleges of Naval State University submitted for Certificate of Program Compliance (COPC) with the College of Education (COED) as the first to receive the certification last October 10, 2016.</p>
		<p class="justify"><span class="margin-3"></span>“So here in Region 8, it’s really a mandatory, it’s really a requirement that all programs must be submitted for COPC plus there is a requirement in the deployment of the OJT, that if the program does not have the COPC yet, the OJT cannot be deployed for their OJT program or for their practicum or for their apprenticeship,” Ms. Elizabeth Ybañez, Member of Quality Assurance and Services Office said.</p>
		<p class="justify"><span class="margin-3"></span>Moreover, she also exclaimed that even in giving the budget they were asked of how many programs that the university have COPC when they have their budget hearing in senate because the more programs with the said certification, the bigger the budget is allotted for the university.</p>
		<p class="justify"><span class="margin-3"></span>“After opening a program you need to have that certification, which means that you have complied the minimum requirement given by CHED but then we just operate and CHED just allows us to operate without it,” Ms. Ybañez added.</p>
		<h6><strong>COED 1st to apply and receive certification</strong></h6>
		<p class="justify"><span class="margin-3"></span>With the initiation of the former Dean of COED, Dr. Victor C. Cañezo, who is now the officer-in-charge University President, the COPCs of the said college were successfully submitted and approved last October 10, 2016.</p>
		<p class="justify"><span class="margin-3"></span>“First two programs to have the certificate are the Bachelor of Secondary Education and Bachelor of Elementary Education of COED because Sir Victor is the Dean that time and he really pushes to have the certification and when he becomes the president, he also pushes to all other colleges to have the certificate of program compliance until it becomes a requirement in CHED,” Ms. Ybañez further said.</p>
		<h6><strong>Univ programs for certification</strong></h6>
		<p class="justify"><span class="margin-3"></span>In addition, Ms. Ybañez stressed that last April 2018, there were 12 programs who voluntarily submitted for the certification and were already visited by the Regional Quality Assurance Team (RQAT) and by that time, there were five programs</p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>