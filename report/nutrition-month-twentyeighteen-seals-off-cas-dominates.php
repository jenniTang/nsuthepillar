<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>Nutrition Month 2018 seals off;<br>CAS dominates</strong></h3>
		<h6 class="text-center"><i></i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>Deafening cheers, pounding beat of music and crowded scene seal off the entire Naval State University Hostel during this year’s 44th Nutrition Month Celebration anchored with the theme “Ugaliing Magtanim, Sapat na Nutrisyon Aanihin,” hosted by the Health and Sciences Department last July 25.</p>
		<p class="justify"><span class="margin-3"></span>“Let us be encouraged to increase consumption of fruits and vegetables to address micro-nutrient deficiency and prevent non-communicable diseases,” Dr. Leomero Garcia, Vice President for Academic and External Affairs stated on his opening remarks.</p>
		<p class="justify"><span class="margin-3"></span>In addition, two major contests highlighted the event, the Jingle Making contest and Mr. and Ms. Healthy Body which comprises the different voices and shouts inside the venue supporting the representatives of their department.</p>
		<p class="justify"><span class="margin-3"></span>College of Arts and Sciences dominated the Nutrition Month 2018, after winning the Jingle Making Contest and Ms. Healthy Body 2018.</p>
		<p class="justify"><span class="margin-3"></span>“During the Nutrition Month Celebration, I can say that it is 95% out of 100% successful. Since this</p>
		<p class="justify">culmination is just a short-notice and we weren’t able to prepare more activities compared last year but we’re glad that it went well,” Mr. Brian T. Ampong, Chairman of Nutri Month 2018, stated during the interview.</p>
		<p class="justify"><span class="margin-3"></span>The CAS department was proclaimed as the Champion, for Jingle Writing Contest followed by the College of Engineering (COE) and  NSU-Laboratory High School as the first and second runners-up, respectively.</p>
		<p class="justify"><span class="margin-3"></span>Meanwhile, in the Mr. and Ms. Healthy Body, Dennis Gabod from College of  Education and Rhejayne Meeka Inocencio from College of Arts and Sciences crowned as Mr. and Ms. Healthy Body, Mr. Stephen Lavado from College of Tourism and Ms. Kathleen Ponce Sabornido from College of Engineering took the 1st runners-up title and Mr. John Gilbert Labandia from College of Engineering and Ms. Desiree Salonoy from College of Education bagged the 2nd runners-up title. Moreover, both Mr. and Ms. College of Engineering was awarded as Best in Sports Wear.</p>
		<p class="justify"><span class="margin-3"></span>“The success of the celebration marks the milestone of history, you have a healthy living and continue living healthy”, Mr. Garcia concluded.</p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>