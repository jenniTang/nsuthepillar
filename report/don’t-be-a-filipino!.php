<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>Don’t be a Filipino!</strong></h3>
		<h6 class="text-center"><i></i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>If I were just a Filipino, I would condemn the directive of President Rodrigo Duterte to apprehend tambays like how most of the critics have been doing. However, it just so happened that I was born as a wise and practical Filipino. So I guess, I’ll just have to start teaching myself to go home earlier than 10:00 PM from now on.</p>
		<p class="justify"><span class="margin-3"></span>Another controversy has yet stirred upon the order of the President Rodrigo Duterte to have a crackdown against loiterers or tambays who, according to him, are “potential troubles for the public”.</p>
		<p class="justify"><span class="margin-3"></span>When I first heard of this, I could not contain myself but to laugh at the idea of apprehending idlers or tambays. What a silly idea of the government, I thought. Yet, as the issue circulated more and become the hottest topic in the mainstream and social media, I have come to understand the point of the law. There, I realized that I was the ‘silly’ and not Duterte. </p>
		<p class="justify"><span class="margin-3"></span>I don’t intend to appear partisan. But while most people and the Human Rights Commission (CHR) took the order of Duterte as an act impeding Human Rights and is without legal basis, I see this as a strategic action of the government to gradually prevent crimes to happen.</p>
		<p class="justify"><span class="margin-3"></span>In Biliran where all municipalities, including Naval, have strictly implemented local ordinances against vagrancy, drinking near the streets, and idlers who are still out at 10:00 in the evening, we can safely say that we are directly affected with the directive of the president.</p>
		<p class="justify"><span class="margin-3"></span>At NSU, which is situated at the heart of the town of Naval, there are night classes that end up to 9:00 in the evening, an hour before the police officers conduct their roving.</p>
		<p class="justify"><span class="margin-3"></span>According to PCOO Undersecretary Mocha Uson, when one chooses to idle around, it shows that he or she is more or less not doing anything productive. It also makes them open to doing things that they are not inclined to do when they are busy with more meaningful activities.</p>
		<p class="justify"><span class="margin-3"></span>This may be a challenge for us not to wander around and lurk in the streets on the town. This may be a constant reminder for us to go directly home or in boarding houses after being dismissed on a night class. Let us be wise and practical and take this ordinance as motivation to just stay at our boarding houses, work on the school projects and other requirements and stuff, and ‘to be productive’ as Uson said, instead of idling around with no sense and be suspected as a criminal.</p>
		<p class="justify"><span class="margin-3"></span>Also, I believe that apprehending tambays does not imply that the government is impeding our rights to move and do what we want late at night. Let us just accept the fact that freedom is a responsibility, that it comes with a duty to respect and uphold the laws of the land. As Uson said, “It is a call for us to practice discipline, as peace and order in the society cannot be done by the President alone, or by the government alone. It is a team effort and requires the action of each and every Filipino.”</p>
		<p class="justify"><span class="margin-3"></span>Internalize it. There’s no wrong about embracing this, right?</p>
		<p class="justify"><span class="margin-3"></span>Now, take the challenge of upholding this law. We may not yet see its implication this time, but who knows what would happen someday. This perhaps would make our country a crime-free place in the future, we just have to see it on its brighter side.</p>
		<p class="justify"><span class="margin-3"></span>So this time, do not just be a Filipino: Be the wise and practical Filipino.</p>
	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>