<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>Change has Come</strong></h3>
		<h6 class="text-center"><i>by Jade Banate</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>Tough. Blunt. Insensitive. Whenever I hear these three words, only one person comes to my mind: President Duterte.</p>
		<p class="justify"><span class="margin-3"></span>Two years ago, Duterte won the Presidential election and sat as the 16th President of the Philippine Republic. And just last July 23, he delivered his third State of the Nation Address (SONA).</p>
		<p class="justify"><span class="margin-3"></span>We have heard the the prime goal of the conduct of a SONA which is to have the report from the President on the accomplishments on his term within the year and plans for the next.</p>
		<p class="justify"><span class="margin-3"></span>And yes, what many of us has expected from the President to deliver has been met as he mentioned his achievements and the current issues of his administration. Listening on every phrases he said and every words he stressed, there is a single thing that caught up my attention: it seemed there is unusual happening, until the end of his speech. I saw a different persona of him- the much better persona. </p>
		<p class="justify"><span class="margin-3"></span>As he talked before  the people, hundreds of law-makers and government officials,  he remained tempered and calm, the unusual thing I saw on him. It made me wonder what happened or what just pushed him to rein himself from being explosive.</p>
		<p class="justify"><span class="margin-3">As remembered on his past SONAs and even in his interviews on the mainstream media, Pres. Duterte is known for his sally and rambling speeches seasoned with  irrelevant jokes, that I could safely say, has made a new mark for his name. </span></p>
		<p class="justify"><span class="margin-3"></span>His speech often hit the right note as he bluntly addressed his administration’s war against drugs and its bloody operations, which he aimed to human rights advocates and church leaders who howls of protest “Your concern is human rights, mine is human lives.” Even when he went through a laundry list of issues, his character remained.</p>
		<p class="justify"><span class="margin-3"></span>I would be honest this time and for this once, I salute President Duterte.  He has done a great job on controlling his habits during the whole SONA. Most of his critiques would probably watch for his explosive words rather than focusing on its content but a surprise when the President showed a different character, a different side of his personality.</p>
		<p class="justify"><span class="margin-3"></span>The formality shown by the President could be counted as one of his achievement not only as the President of this country but for his life career as it conforms to common good.</p>
		<p class="justify"><span class="margin-3"></span>Generally, the president’s third SONA went well; so much better indeed. The criticized and unstatesman personality of the President was gone. He changed his track from being explosive to being calm. </p>
		<p class="justify"><span class="margin-3"></span>Tempered.Restraint. Straightforward. The new characteristics that the President possesses.</p>
		<p class="justify"><span class="margin-3"></span>I could not help but to recall his infamous tagline ‘Change is coming.’ But after his SONA, it may be not the country but for the change of himself has already come. </p>
	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>