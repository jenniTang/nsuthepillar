<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>NSU ULRC Gearing Towards Modernization</strong></h3>
		<h6 class="text-center"><i>by Deal Joseph Morillo</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>The NSU University Learning Resource Center is taking actions for the modernization of its facilities as well as the expansion of the building to better serve the students and the staff of NSU.</p>
		<p class="justify"><span class="margin-3"></span>“Last August 2017, as you can remember, we had conducted a book fair to procure more books which is part of the PCDP or Procurement Concern Development Plan of the library to buy new books for all programs to serve as reference for students and teachers alike,” Dr. Connie Gayrama, NSU University Librarian, stressed in an interview.</p>
		<p class="justify"><span class="margin-3"></span>The library, which is situated at the fringe of the university, has huge enough space to accommodate the resources for the current year; however, due to the increase in population brought about by the K-12 program, furnishing more space for the growing population is a huge concern to effect the library’s modernization.</p>
		<p class="justify"><span class="margin-3"></span>“As of now, due to the limited space in the campus, we have opened the library in Engineering and Maritime Education Departments to support the university’s growing population. Hopefully, this side at the back of the library would serve as an expansion,”Dr. Gayrama clarified. “That is why I am appealing to the administration to give this small space for the library’s expansion because it is also part of the student services.”</p>
		<p class="justify"><span class="margin-3"></span>Dr. Gayrama also stressed the importance of bigger reading area for the library users: “There are no specific requirements for the number of books to be housed in the library, what we need now is the reading area. Small reading area prevents the students from utilizing the library due to the inadequacy of space.” “Sufficient supply of books and comfortable reading space should go hand in hand because if you have a huge reading area but you do not have enough number of books, then it would be useless. The users, reading area and the resources are all important to the smooth operation of the university library,” she further explained.</p>
		<p class="justify"><span class="margin-3"></span>In order to fully implement the modernization of the library, it targets to install Wi-Fi connectivity in the ground floor of the building. “The Wi-Fi connection also depends on the power capacity. The upper area has a strong connection while the ground floor has a weaker internet connection. But then again, even if we add more Wi-Fi connectivity in the library, we cannot guarantee reliable and strong connection due to the huge number of students using our connection. So we really need a more stable connection,” she clarified.</p>
		<p class="justify"><span class="margin-3"></span>One of the few ways that the university library caters the needs of the students is extending its operation time from 5 to 7 P.M. The University librarian said that one of the standards of the college library is to have 60-hour operation a week. “Aside from that standard, there are also requests that the library should be open until 5 in the afternoon.</p>
		<p class="justify"><span class="margin-3"></span>‘‘However, I have observed that there are not much students inside the library beyond 6 pm. Besides, there are usually only one to two students utilizing the second floor which is a waste of the electricity. That is why I have decided to close the second floor at 6 pm; anyways, they can bring books they need downstairs in order to save electricity,” Dr. Gayrama stressed.</p>
		<p class="justify"><span class="margin-3"></span>The university librarian shared how the Administration was very supportive of the plans of the ULRC. “Our next target is the expansion of our library because we almost have it all kai ang ato administration, very supportive sa tanang affairs sa university library. Imagine nag book fair ta, 2.5 million ang gi approve sa Board. We also bought security gate worth 2.5 million as well, so our purchases are geared towards modernization. We are also the first in the region to operate an RFIT security gate,”  Dr. Gayrama expounded.</p>
		<p class="justify"><span class="margin-3"></span>She also stressed the importance of printed materials and encouraged the university users especially the students to visit the library more often and utilize its resources from books to the free personal computer utilization. “We should not always depend information on the Internet because printed materials are much more reliable and the ability to retain information is better when you are reading printed materials compared to E-Books and online websites. Even our administration is very supportive with the library so we need to encourage more students to utilize and visit the library more often because we have so many books waiting to be read and explored,” Dr. Gayrama concluded.</p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>