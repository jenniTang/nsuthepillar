<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>Studs join Wet ‘n Wild Acquaintance Party</strong></h3>
		<h6 class="text-center"><i>by Abegail Mondelo and Joevenil Jamin</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>NSU grounds turned into a wild space as Naval State University once again celebrated its Annual Acquaintance Party with the “Paint Party” theme that turned into “Wet and Wild” as it rained last July 11 at the NSU Oval Grounds.</p>
		<p class="justify"><span class="margin-3"></span>“We gave our best shot in organizing this program intended for you, our dear students, because we believe that NSU is not only for us but also for all of you. So in return, we want you to relax, meet new faces and don’t forget to make most of the night,” SASO Director Dr. Edwin G. Salvatierra said in his Welcome Address.</p>
		<p class="justify"><span class="margin-3"></span>The field transformed into the dancefloor of the students of NSU Cultural Groups, NSU Panamao Dance Company, the HEARTHROBS and New Grounds as they showcased their moves in a various of dance number.</p>
		<p class="justify"><span class="margin-3"></span>The Induction Ceremony of the elected Federation of Student Council FSC, Supreme Student Council SSC and Classrooms Officers was one of the highlights of the event.</p>
		<p class="justify"><span class="margin-3"></span>“Tonight will be another remarkable evening to all of us because this is an official event that will mark again in the history of NSU as we will induct our new set of elected officers for the Student Council, so as I speak, I would like to congratulate each and every one of you,” Dr. Salvatierra further said.</p>
		<p class="justify"><span class="margin-3"></span>In addition, Hon. Roger J. Espina Jr., Congressman of Biliran, Hon. Gerardo J. Espina Jr., Provincial Governor, Hon. Gerard Roger M. Espina, Municipal Mayor of Naval, Dr. Victor C. Cañezo Jr., NSU OIC University President and Key Officials graced the event.</p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>