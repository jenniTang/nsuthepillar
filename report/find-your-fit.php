<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>FIND YOUR FIT</strong></h3>
		<h6 class="text-center"><i>By Joshua Gibson Fuentes</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->
		<p class="justify">Empowering students to join organizations and participate on its activities has been one of the highlights of the NSU’s banner program dubbed as ‘NSU is YOU’.</p>
		<p class="justify"><span class="margin-3"></span>As soon as the school year kicked off last June, different student organizations has started setting off their drives and own way of adverting students’ attention and recruiting some prospect members. </p>
		<p class="justify"><span class="margin-3"></span>But hey, out of the more than 30 registered student organizations in the university, have you already made up your mind to which org you want to be part of? I bet you still have not. Well, let me give you some sort-of-info of some of the major orgs in the campus. </p>
		<p class="justify"><span class="margin-3"></span>So c’mon, read this, be acquainted of these clubs and together, LET’S FIND YOUR FIT!</p>
	
		<h6><strong>1. Federation of Student Council, Inc.</strong></h6>
		<p class="justify">If you are a student with a heart of a true leader, welcome to FSC!</p>
		<p class="justify"><span class="margin-3"></span>Federation of Student Council (FSC), the home of students driven by a strong passion and spirit of leadership, has preserved the culture of promoting and protecting the students’ rights and welfare.</p>
		<p class="justify"><span class="margin-3"></span>On its very humble beginning more than 20 years ago, the organization was formerly called as Naval Institute of Technology (NIT)- Supreme Student Council (SSC). However, upon the conversion of the then NIT into a State University last 2010, the law required a federation of Student Councils to represent the students to the Board of Regents. Thus, the establishment of the NSU – Federation of Student Council, Inc. </p>
		<p class="justify"><span class="margin-3"></span>While the recruitment of the members of the organization is done upon the enrolment of every student in the university, the officers to lead the studentry are chosen through a university-wide election every February of the year. </p>
		<p class="justify"><span class="margin-3"></span>Recently, FSC, through the leadership of its president Student Regent Hon. Jerome T. Arcenal, has made marks and altered the history of NSU as the organization moved front-row on the realization of the 4-Day Class Policy, acquisition of the NSU coaster; and became supplemental on the preparation of the National Student Initiative to petition the government to release the 9 Billion free education budget to Filipino students. Also, the organization was the only council to submit to Congress a manifesto which in result, the approval of the RA 10931 or the Universal Free Education Act.</p>
		
		<h6>The Pillar Publication</h6>
		<p class="justify"><span class="margin-3"></span>The Pillar Publication, the official tertiary publication of NSU, is where campus journalists belong. Driven with the innate goal of promoting the welfare of the students and the just dissemination of information, the publication has continually served the very purpose of its existence.</p>
		<p class="justify"><span class="margin-3"></span>Pillar is defined as a large post that helps to hold up something. Philosophically, the publication, for years, has held an important role on upholding the legacy carved into the history page of NSU.</p>
		<p class="justify"><span class="margin-3"></span>On its 29 years of service since its establishment on the year 1988 as one of the leading student organizations in the campus, The Pillar has evidently contributed service to the institution and to the studentry. It had even transcended its role from a mere publication into a prime mover of change as it advocates students’ empowerment, bringing the NSU to higher grounds.</p>
		<p class="justify"><span class="margin-3"></span>Proving that the publication is a powerhouse of competitive and skilled student-journalists, The Pillar has given honor to the university by winning different regional competitions like the Campus Press of the Year Award on the Regional Tertiary Press Conference (RTSPC) last 2013, and bagging major awards on various national competitions.</p>

		<h6>NSU-An Lantugi Debate Society</h6>
		<p class="justify"><span class="margin-3"></span>Members of this organization are trained to be a warrior, to stand firmly on their position, and to be a victor in every word war they’ll face. An Lantugi Debate Society (ALDS), or some may call as DebSoc, is the home of individuals driven with a strong desire of voicing out ideas to captivate listeners to believe on them and conform on what they are fighting for.</p>
		<p class="justify"><span class="margin-3"></span>Aiming to boost the members’ critical minds, the organization conducts and participates to different debate seminars, cross-trainings with other universities in the region, and mock debates that will promote their academic capacity of rational thoughts in exchange and construction of verbal ideas on how conversation is thrown giving a big impact to the diverse edge.</p>
		<p class="justify"><span class="margin-3"></span>Armored with thoroughly sharpened thinking, DebSoc has clutched different awards on the local, regional and national level of debate wars. Amongst these are: Grand Championship on the years 2012, 2014, and 2015; Pre-finalist (2013) and Semi-finalist (2012) during the Eastern Visayas Debate Championship (EVDC). </p>
		<h6>Panamao Dance Company</h6>
		<p class="justify"><span class="margin-3"></span>Hip-hop, classic, contemporary, folk. Name any dance, they’ll surely give it. Panamao Dance Company, with its three main divisions: Folkloric Group, Hip-hop Group, and Dance Sport Team, is named after the old name of the enchanted island of Biliran Isla de Panamao. The emergence of the group was because of the interfusion of the two old dance group: NSU Dance Society and Old School last 2006.</p>
		<p class="justify"><span class="margin-3"></span><strong>Folkloric Group.</strong> The ability to hold their audiences’ attention is the Folkloric group’s métier. From ritual, tribal, and ethnic dances of the diverse culture of the indigenous people of the Cordillera Mountains and Mindanao to the lively and vibrant songs and movements of the lowlanders, folkloric group of the Panamao Dance Company has served to be the advocate of conserving and giving life and meaning to the unending culture of the native tribes across the country.</p>
		<p class="justify"><span class="margin-3"></span><strong>Hip-hop Group.</strong> Bringing their vigorous yet fierce appeal and readied with amazing moves and stunts, the Panamao Dance Company Hip-hop Group dancers do not fail to excellently show off their unique style of dancing. Giving off their best shot, they always conquer every dance floor they step to. Last 2016, the group was declared champion on the Hip-hop competition during the Philippine Association of State Universities and Colleges (PASUC) Regional Culture and the Arts Festival, and bagged the eighth place in the national level held in Vigan, Ilocos Sur.</p>
		<p class="justify"><span class="margin-3"></span><strong>Dance Sport Team.</strong> Truest to its very aim of bringing awareness of how important dance sport is in the university as well to its neighboring towns and provinces, Dance Sport team has made a promising journey as the group accomplished a feat as the third placer for winning both the Latin and American standard category during the State Colleges and Universities Athletic Association (SCUAA) 2016.</p>
		<p class="justify"><span class="margin-3"></span>As music is a lot more than a soundtrack but an integral part of everyone’s lives, Naval State University did not stop on giving acknowledgement to students with covert prowess in singing and empower them to join organizations designed to harness their talents.</p>
		<p class="justify"><span class="margin-3"></span>NSU Chorale, the official and only existing choral group for students in the university, started at early 2010. With their pleasing and angelic harmony, they continue to delight audiences with their expressive rendition of varied songs. </p>
		<p class="justify"><span class="margin-3"></span>From the single jovial songs to medley of solemn genre of tracks, NSU Chorale has not failed making every note touch every listener’s heart and soul. In fact, with their artistic way of delivering the song, the group finished 2nd runner-up in the Regional PASUC culture and the arts festival chorale singing category. </p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>