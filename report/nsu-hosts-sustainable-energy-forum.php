<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>NSU hosts Sustainable Energy Forum</strong></h3>
		<h6 class="text-center"><i>by Ronalyn Ribot</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>To bring knowledge and movement towards energy conservation, the Action for Economic Reforms and Friedrich Elbert Stiftung brings a sustainable energy forum entitled “Renewable and Sustainable Energy for Industrial Development: Engaging the Transition Towards a Greener and Resilient Future” at Naval State University on July 24.</p>
		<p class="justify"><span class="margin-3"></span>Continuing the conversation on sustainable energy, industrial policy, and community resilience, the forum brought together the members of the academe, representatives for industry, local government units, regional agency officials, and relevant members of the community.</p>
		<p class="justify"><span class="margin-3"></span>“Our national government focuses on sustainable development goal, one of this is the sustainable and renewable energy. We need to engage in having renewable energy like hydro energy and solar energy because we have the source in our province,” Dr. Christopher P. Vicera, Panel Moderator and Director of Extension, said in an interview.</p>
		<p class="justify"><span class="margin-3"></span>Conferred by the Visayas State University VSU and National Economic and Development Authority NEDA- Region 8, the forum aimed to provide relevant information on the current situation of the renewable and sustainable energy initiative and policies with specific focus, foster interaction and exchange among regional agencies, local government units LGU’S , the academe and civil society on how to achieve a greener and resilient communities.</p>
		<p class="justify"><span class="margin-3"></span>There were two topics that the forum intended to impart, the Industrial Policy, climate change and Energy-Charting a  Sustainable Future and Exploring Options for Sustainable Energy-learning From Success in Localized Sustainable Energy Systems.</p>
		<p class="justify"><span class="margin-3"></span>“We need to consume or use renewable energy for us to utilize not just our finances but to conserve energy because that is one of our greatest resources particularly in Biliran,” the Panel Moderator concluded in an interview.</p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>