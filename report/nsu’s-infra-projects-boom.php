<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>NSU’s infra projects boom</strong></h3>
		<h6 class="text-center"><i>by Julita Abrigo and Melanie Betcher</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>As part of new administration’s strategic direction, NSU isYOU, specifically on the Modernization of Facilities, Naval State University, through the Engineering and General Services Office (EGSO) simultaneously launched a number of infrastructure projects for the School Year 2017-2018 and 2018-2019 amounted up to 312M.</p>
		<p class="justify"><span class="margin-3"></span>As shown in the  report of EGSO, a total of Php130M was allotted for the 2017 infra projects including the Repair and Improvement of Structure/Facilities and Acquisition of Equipment, School Water System, Multi-Purpose Building, NSU Technology Building Phase II, Student Center Phase I and II; NSU Dormitory Phase I and II, NSU Gym Phase I, and Academic Buildings.</p>
		<p class="justify"><span class="margin-3"></span>“Most of the projects were already 100% complied, other projects such as Student Center Phase III is about 60% complied and the NSU Dormitory will be finished this August 2018. Also, we are very much proud that we are the first university in Region VIII to have an air conditioned gymnasium,” Engr. Carlito Orais, Director of EGSO, said.</p>
		<p class="justify"><span class="margin-3"></span>Furthermore, this year’s building construction and planned projects amounted to Php82,534,000 including the Continuation Construction of Maritime Training Center (on process with 10M budget), NSU Technology Building Phase III (on bidding process and to be submitted to the board for approval), Academic Building (NSU Main) and Maritime Annex Building (38.54% as of June 25, 2018), NSU Gym Phase II (15% as of July 17, 2018), Student  Phase III; and Additional Sport Facilities.</p>
		<p class="justify"><span class="margin-3"></span>Moreover, according to Engr. Orais, for the year 2019, the university, through the EGSO is planning for another set of projects. A 5-storey Tourism Building and the new Administration Building are already provided with design for the proposals.</p>
		<p class="justify"><span class="margin-3"></span>“When you graduate and come back here, you’ll be able to see an improved and new NSU with modernized buildings and facilities that will accommodate the needs and provide comfort for all the future NSUians,” Engr. Orais concluded.</p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>