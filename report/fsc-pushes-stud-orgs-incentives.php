<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>FSC pushes stud orgs incentives</strong></h3>
		<h6 class="text-center"><i>by Joshua Gibson Fuentes</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>To substitute the vacated scholarships with the implementation of the Republic Act 10931 otherwise known as Universal Free Higher Education Act, Federation of Student Council (FSC) proposed for a monetary incentive for members of student organizations in the university.</p>
		<p class="justify"><span class="margin-3"></span>“We are in the process of asking permission to give incentives for our students in the organizations. This is a way to encourage them to do their duties and responsibilities because as of now, the benefits of ordinary students and organization members has no difference,” Hon. Jerome T. Arcenal, Student Regent and President of the FSC, said.</p>
		<p class="justify"><span class="margin-3"></span>The proposal of the said incentive will be presented to the Board of Regents on the next board meeting.
		<p class="justify"><span class="margin-3"></span>“The proposal is already ready and subject for presentation to the board. We are only waiting for the full implementation of the RA 10931 because up to now, the hearing for the RSS (Return Service System) still continues. All our proposals are pending,” he added.</p>
		<p class="justify"><span class="margin-3"></span>Moreover, he also emphasized that all organizations may qualify to the incentives depending on their accomplishment or performance. Some of the these are the Supreme Student Council (SSCs) of the different colleges, FSC and The Pillar Publication.</p>
		<p class="justify"><span class="margin-3"></span>In addition, The Pillar Publication members, as stated in their Constitution and By-laws, like the SSCs and FSC are entitled for scholarship grants to free tuition.</p>
		<p class="justify"><span class="margin-3"></span>However, according to Ms. Jenny A. Genoguin, adviser of The Pillar, students are becoming less engaged with extra-curricular activities most especially no scholarship is provided anymore for them because of the implementation of free education.</p>
		<p class="justify"><span class="margin-3"></span>“Incentivizing our students who are actively involved in organization is strategic for it will help encourage and motivate students to be involved with academic and non-academic activities,” she said.</p>
		<p class="justify"><span class="margin-3"></span>Furthermore, the amount that organization members may get is dependent on the budget both of the student org, publication fund, and Student Development Fund (SDF).</p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>