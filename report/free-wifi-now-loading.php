<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>Free WIFI: Now Loading</strong></h4>
		<h6 class="text-center"><i>By Tonette Grace Orbena</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>“Ingon nay free wifi, wa pa lage? Last year pa man to...”</p>
		<p class="justify"><span class="margin-3"></span>Students of the university, especially from last year, all cry these same words. Last year, it was said that there will be free wifi for all of the university’s stakeholders. However, queries then followed when the realization of this project became unrealized. </p>
		<p class="justify"><span class="margin-3"></span>As students of this generation, we now treat internet as a necessity. So, when the announcement was made, we were, for a moment, felt blessed. After that, months passed, or even maybe a year, we have been waiting and waiting for it. So where is it now? If it will really be installed, why the delay?</p>
		<p class="justify"><span class="margin-3"></span>To answer all those questions, I have interviewed Mr. Reymon Santieñez, System Administrator,a Management Information System (MISU). According to him, the main cause of the delay is that PLDT and Globe, our supposed-to-be service providers, offering 10 mbps, was contracted about the said project but didn’t give their reply after a couple of months passed. Fortunately, a company named IXS, which is also the service provider of LNU, visited our school just last week and offered to be our service provider. It was said that they propose greater amount of data to be used, which is 20 mbps. If there is already an internet, it is planned that each student will have an allotment of 2 hours of usage per day.</p>
		<p class="justify"><span class="margin-3"></span>As some of us have known (or maybe not), there are already wifi routers that are installed throughout the university – in AVR Andaya Building, Library, College of Education building, College of Engineering building and NSU Gymnasium. He added that the preparations are already finished and they are just waiting for the full internet connection which will be provided by the service provider. As of now, the papers are still being processed and they are hoping that this project will be finished within this semester. Moreover, they will inform the students and a launching activity will be held once the preparations are complete.</p>
		<p class="justify"><span class="margin-3"></span>It is a relief to know that the management is really working on it and not cutting some slack, of some of us may think. The only problem is that, they lacked communication to the students. If that was the problem, they should have informed us earlier so that we wouldn’t have said something that could question their commitment to this project. For the reason that it would leave an impression to the students that the administration is not true to their word.</p>
		<p class="justify"><span class="margin-3"></span>For the students, it is always better to know the reason of the occurrence of a certain problem, which is now, the delay of the free wifi as an example. It is okay to ask, but not to the extent that we complain too much and criticize without even knowing the real situation.</p>
		<p class="justify"><span class="margin-3"></span>In this light, it’s not really the fault of neither the administration nor the students. It’s just the because of an unexpected situation to the service provider we have applied. As of now, students are expected to wait a little longer while it is still processed. We hope that this time, the said promise will not be broken, but instead to be proven.</p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>