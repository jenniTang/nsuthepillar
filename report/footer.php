</div><!-- /.container -->
</section><!-- / #ccr-main-section -->

<aside class="ccr-footer-sidebar col-md-12 col-sm-12" style="padding-bottom: -5em;"><br>
	<div class="row">
		<div class="text-center" id="contactUs"><h2 class="contact-us-title">CONTACT US</h2></div><br>
			<div class="col-md-1 col-xs-0"></div>
			<div class=" col-md-5 col-sm-6 col-xs-12 col w3-animate-opacity">  
				<div class="offset-xs-1">
					<div class="col-md-11 contact-us-title">
						<p>Facebook Page: <a href="https://www.facebook.com/nsuthepillarpub/" target="_blank">https://www.facebook.com/nsuthepillarpub/</a></p>
						<p>Address: Naval State University - Naval, Biliran </p>
						<p>Contact Number: +639123456789</p><br>
					</div>
					<div class="col-md-12"><br>
						<iframe class="col-md-12 col-xs-12" height="275" id="gmap_canvas" src="https://maps.google.com/maps?q=naval%20state%20university%20naval%2Cbiliran&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
					</div>
				</div><br>
			</div>
			<div class="contact-us col-md-5 col-sm-6 col-xs-12 w3-animate-opacity">
				<div class="col-md-12 offset-xs-1">
					<?php
						if(isset($_GET['s'])){
							$s = $_GET['s'];
							if($s==1110101011101){
								$send = "<p class='alert alert-success col-md-12'>Message has been sent.</p>";
							}else{
								$send = "<p class='alert alert-danger col-md-12'>Message is not sent.</p>";
							}
							echo "<br>".$send;
						}
					?>
					<form action="../vendor/mailer.php" method="post"><br>
							<label><i><strong>Name*</strong></i></label>
							<input class="form-control" type="test" name="name" placeholder="Name" required="">
							<label><i><strong>Your Email*</strong></i></label>
							<input class="form-control" type="text" name="email" placeholder="Your Email" required="">
							<label><i><strong>Message*</strong></i></label>
							<textarea class="form-control" style="height: 100px" name="msg" placeholder="Message Here." required=""></textarea>
							<input class="btn btn-primary pull-right" style="margin-top:10px;" type="submit" name="send" value="SEND">
							<br><br>
					</form>
				</div>
			</div>
	</div><br>
		<div class="col-md-12">
			<div class="read-more text-center" style="margin-bottom: 0em;">
				<p>Do you want a copy for this publication?
				<input type="button" value="Print this page" onclick="printPage()" /></p>
			</div>
		</div>
	<br>
</aside> <!-- /#ccr-footer-sidebar -->


<footer id="ccr-footer">
	<div class="container pull-right" style="padding-right: 10px;">
	 	<div class="copyright">
	 		All Rights Reserve <u>The Pillar</u>
	 		&copy; 2018,  <a href="http://jennitang.com" target="_blank">jenniTang</a> works.
	 	</div> <!-- /.copyright -->

	 	<div class="footer-social-icons">
	 	</div><!--  /.cocial-icons -->

	</div> <!-- /.container -->
</footer>  <!-- /#ccr-footer -->
	
	<script>

	var rm = 'Read More';
	var m = 'Minimize'
		document.getElementById('readMore').innerHTML = rm;
		function myAccFunc(id) {
		    var x = document.getElementById(id);
		    if (x.className.indexOf("w3-show") == -1) {
		        x.className += " w3-show";
		        document.getElementById('readMore').innerHTML = m;
		    } else {
		        x.className = x.className.replace(" w3-show", "");
		        document.getElementById('readMore').innerHTML = rm;
		    }
		}

		
	</script>
	<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>
