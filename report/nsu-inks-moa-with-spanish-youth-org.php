<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>NSU inks MOA with Spanish youth org</strong></h3>
		<h6 class="text-center"><i>by Rommel Cayon</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>To forge partnership with the Mission Cebu of Madrid, Spain (MCMS), a group of Spanish youth missionaries, Naval State University signed a Memorandum of Agreement (MOA) during the welcome program hosted for the organization held at the NSU Hostel, July 20.
		<p class="justify"><span class="margin-3"></span>“That is to lock our agreement, meaning to say, that is the bible of our actions. What are written in that agreement, we subscribe to that,” Thelma Gadugdug, COED Extension Coordinator, emphasized.</p>
		<p class="justify"><span class="margin-3"></span>MCMS is a non-governmental organization that contributes to poverty alleviation and empowerment through the provision of literacy, values formation, social protection, and the promotion of rights and welfare of the poor through their policies, programs, projects, and services implemented with or through Local Government Unit (LGU’s), State Colleges and Universities (SUC’s), Non-Government Organization (NGO’s), People’s Organization (PO’s), Other Government Organizations (GO’s) and other members of civil society.</p>
		<p class="justify"><span class="margin-3"></span>The purpose of the said MOA is to provide framework for a partnership agreement between MCMS and NSU to implement a series of Literacy Education, Values Formation and Livelihood Skills Development that will enable poor families to acquire skills leading to micro-enterprise development and employment facilitation.</p>
		<p class="justify"><span class="margin-3"></span>“The extent of the MOA will enable our group to teach Spanish lessons of our culture and literature, share livelihood programs and help NSU’s services extended to the community,” Victor Cordova, leader of the MCMS, concluded.</p>
		<p class="justify"><span class="margin-3"></span>Moreover, MCMS also helped the school and little chapel of Brgy. Pulang, Bato, Almeria, according to them, the fund that they used is from their own hard-work and donations.</p>
		<p class="justify"><span class="margin-3"></span>NSU and MCMS both recognize the need to strengthen the economic capacity on literacy education, values formation, livelihood skills and development and other extension projects and activities for the beneficiaries by developing their skills through livelihood skills development.</p>

	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>