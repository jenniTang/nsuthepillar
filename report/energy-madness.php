<?php include'header.php';?>

<section id="ccr-left-section" class="col-md-8 col-sm-6 ccr-home">

	<div class="col-md-12" id="read">
		<h3 class="text-center"><strong>Energy Madness</strong></h4>
		<h6 class="text-center"><i>By Rogelio Dimakiling, Jr.</i></h6>
		<section class="bottom-border" style="margin-bottom: 0em; margin-top: -1em;">
		</section> <!-- /#bottom-border -->

		<p class="justify"><span class="margin-3"></span>A clear solution or a firm objection, Almeria takes blows as bets on its geothermal resources rages until the final verdict.</p>
		<p class="justify"><span class="margin-3"></span>The island has never been this fired up as issues and possible answers that wish to address energy crisis in the province as well as Region VIII continue to elevate between the local government and BGI.</p>
		<p class="justify"><span class="margin-3"></span>But why geothermal? Biliran, for all we know, is a small, volcanic, and a less populated island province. These characteristics suit the main requirements of the operation. Steams of gases are harvested from below surface and with the island possessing volcanic features, it is the perfect solution to the power crisis we are all facing. Also, the land allotment of geothermal plants takes much smaller size than other energy reservoirs such as oil, water, wind and nuclear energy. With such obvious feats, I see no negation to occupy an adequate portion in the soils of Almeria.</p>
		<p class="justify"><span class="margin-3"></span>We need energy, we always do. And vying for the fact that granting access to tap unto Mother Earth’s natural resources, should it sustain and stabilize the locality’s electrical power supply, it is, therefore, a consideration to make. I can never argue with myself when I get infuriated by power losses, sudden blackouts, and even expensive bills where customers like me and maybe most of us are less satisfied as time goes by. Thus, the common feel of the need of change is quite protruding.</p>
		<p class="justify"><span class="margin-3"></span>After all, the government partnered with the energy sector continues to search for the best choices as they take steps towards progressive development. Such advantages offered through the geothermal operation can be overlooked as an asset of economic growth for both local and international businesses and tourism, gaining the much more distinguished title of from-undiscovered-to-unveiled paradise. If it really brings positive outcomes which poses a good reputation to our community and neighboring provinces, then why not pursue it?</p>
		<p class="justify"><span class="margin-3"></span>One of the more reasons that energy mining is crucial, reflects to its own surroundings. We all are very careful in regards to the safety of everyone and with that in mind, I could never miss the continual existence of the Tanauan plant, which I personally believe is still powering our homes and lives. Its success story serves as the basis to where this energy solution can take Biliran to the next level. I really do hope that our higher ups can envision this in the busy days to follow.</p>
		<p class="justify"><span class="margin-3"></span>Furthermore, negligence of opportunity will leave us hanging and waiting for another solution which I presume would take much more effort to achieve. I say, BGI should be approved to continue its next phase of operation not only in Almeria but also in other fertile parts of Biliran.</p>
		<p class="justify"><span class="margin-3"></span>Whether a yes or no, it all boils down to the decisions of the many. Without the necessary information, the public will never understand that the purpose of the geothermal operation benefits the greater good of Biliranons. It is time to end this energy madness, once and for all.</p>
	</div>

	<section class="bottom-border2">
	</section> <!-- /#bottom-border -->

	<section class="bottom-border">
	</section> <!-- /#bottom-border -->

</section>

<?php include'footer.php'; ?>