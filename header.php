<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="" >
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>NSU | The Pillar</title>
	<link rel="icon" href="img/pic_release/pillar_logo2.png">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/w3.css">

	<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
	<![endif]-->

</head>
<style>
.read-more input:hover{
	background-color: rgba(0,0,0,0.05);
	color: rgba(128,128,128,0.5);
	-webkit-transition: all .15s ease-in;
	   -moz-transition: all .15s ease-in;
	    -ms-transition: all .15s ease-in;
	     -o-transition: all .15s ease-in;
	        transition: all .15s ease-in;
}
.read-more input{
	color: rgba(0,0,0,0.9);
	background-color: rgba(0,0,0,0.075);
	display: inline-block;
	padding: 0px 10px;
	line-height: 30px;
	border: 1px dashed rgba(0,0,0,0.5);
}
.contact-us-title{
	color: #fff;
}
.contact-us-title p{
	color: #fff;
}
.contact-us{
	color: #fff;
	background-color: rgba(0,0,0,0.075);
	display: inline-block;
	padding: 0px 10px;
	line-height: 30px;
	border: 1px dashed rgba(0,0,0,0.9);
}
/*.contact-us2{
	color: #fff;
	display: inline-block;
	background-color: none;
	padding: 0px 10px;
	line-height: 30px;
}*/
.bottom-border2{
	width: 100%;
	padding-top: 20px;
	margin-bottom: 10px;
	border-bottom: 1px solid #ccc;
}

.btn:hover{
	background-color: #e6e6fe !important;
	color: rgba(0,0,0,0.9);
	-webkit-transition: all .15s ease-in;
	   -moz-transition: all .15s ease-in;
	    -ms-transition: all .15s ease-in;
	     -o-transition: all .15s ease-in;
	        transition: all .15s ease-in;	
}

.pre-title{
	background-color: rgba(182, 181, 96, 0.05);
	color: rgba(0,0,0,0.4) !important;
}
.margin-3{
	margin-left: 3em;
}
.margin-1{
	margin-left: 1em;
}
.justify{
	text-align: justify;
	margin-top: 10px;
}
.ccr-footer-sidebar{
	background-color: #282c2d;
	background-image: url(img/footer-sidebar-bg.png);
	width: 100%;
}
#info-body{
    background-color: #f0f8ff !important;
    border: 1px solid rgba(182, 181, 96, 0.30); 
    margin-top: 1em;
    border-radius: 0px 0px 5px 5px;
    box-shadow: inset 0 1px 10px 3px rgba(182, 181, 96, 0);
}
</style>
<script>

	function printPage() {
	    window.print();
	}

	function startTime() {
		var today = new Date(Date.now());
		var h = today.getHours();
		var m = today.getMinutes();
		var s = today.getSeconds();
		m = checkTime(m);
		s = checkTime(s);
		document.getElementById("txt").innerHTML= h+":"+m+":"+s;
		var t = setTimeout(startTime, 500);
	}
	function checkTime(i){
		if(i<10){i = "0" + i};
		return i;
	}

	var myVar = setInterval(myTimer, 1000);
	function myTimer() {
	    var d = new Date();
	    document.getElementById("localTime").innerHTML = d.toLocaleTimeString();
	}
</script>
<body onload="startTime()" style="background-color: #e6e6fe;">


<header id="ccr-header">
	
	<section id="ccr-site-title" style="background-color: rgba(74,73,143,75); background-image: url(img/footer-sidebar-bg.png);">
		<div class="container">
			<div class="site-logo">
				<a href="index">
					<img src="img/pic_release/pillar_logo.png" alt="Side Logo"/>
						<h1 class="w3-animate-right">THE <span>PILLAR</span></h1>
						<h6 class="w3-animate-right">Official Student Publication of NSU</h6>
				</a>
			</div> <!-- / .navbar-header -->
		</div>	<!-- /.container -->
	</section> <!-- / #ccr-site-title -->


	<section id="ccr-nav-main">
		<nav class="main-menu">
			<div class="container" >
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".ccr-nav-main">
			            <i class="fa fa-bars"></i>
		          	</button> <!-- /.navbar-toggle -->
		          	
				</div> <!-- / .navbar-header -->
				<div class="collapse navbar-collapse ccr-nav-main text-center">
					<ul class="nav navbar-nav">
						<!--  id="ccr-nav-main" class="active" -->
						<li><a href="index#news">NEWS</a></li>
						<li><a href="op-ed#op-ed">OP-ED</a></li>
						<li><a href="feature#feature">FEATURE</a></li>
						<!-- <li><a href="#">COLUMNS</a></li> -->
						<li><a href="sports#sports">SPORTS</a></li>
						<li><a href="about-us#about-us">ABOUT US</a></li>
						<li><a href="#contactUs">CONTACT US</a></li>
					</ul> <!-- /  .nav -->
				</div><!-- /  .collapse .navbar-collapse  -->
			</div>	<!-- /.container -->
		</nav> <!-- /.main-menu -->
		<div id="currentTime">
			<h5 class="pull-right current-time" id="localTime" style="margin-top: -1px;"></h5><h5 style="margin-top: -1px;" class="pull-right current-time"><?=date('[D] M d, Y');?><i class="fa fa-clock-o margin-1"></i></h5>
		</div>
	</section> <!-- / #ccr-nav-main -->


<section id="ccr-main-section news">
	<br>
	<div class="container">
		<aside id="ccr-right-section" class="col-md-4 col-sm-6">
			<section id="ccr-find-on-fb">
				<div class="find-fb-title">
					<span><i class="fa fa-facebook"></i></span> Find us on Facebook
				</div> <!-- /.find-fb-title -->
				<div class="find-on-fb-body">
					<a target="_blank" href="http://www.facebook.com/nsuthepillarpub/">http://www.facebook.com/nsuthepillarpub/</a>
					<p style="margin-top: 10px;">You can also visit:</p>
					<a target="_blank" href="https://www.facebook.com/NSUisYOU/">https://www.facebook.com/NSUisYOU/</a>
					<div class="fb-like-box" data-href="#" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
				</div> <!-- /.find-on-fb-body -->
				<br>
			</section> <!-- /#ccr-find-on-fb -->

			<section>
				<div class="ccr-gallery-ttile">
					<span></span> 
					<p class="w3-animate-zoom"><strong style="font-size: 15px;">Auditor endorses NSU for ISO 9001: 2015 cert</strong></p>
				</div> <!-- .ccr-gallery-ttile -->

				<div class="sidebar-entertainment" id="info-body" style="margin-top: 1px;">
					<div class="col-md-12 w3-animate-opacity">
						<p class="justify"><span class="margin-3"></span>International Organization for Standardization (ISO) Lead Auditor Joy Cerrafon endorsed Naval State University for Certification for ISO 9001:2015 during the closing meeting of the 2-day Det Norskie Veritas (DNV-GL) Audit last July 20 held at the NSU Hostel.</p>
						<p class="justify"><span class="margin-3"></span>“The auditor will recommend the organization for certification for ISO 9001:2015. The next audit, which is a Monitoring Periodic Audit is scheduled next year.” Ms. Cerrafon said.</p>
						<p class="justify"><span class="margin-3"></span>Mr. Carlito Cabas, Director of Quality Assurance and Accreditation (QAA), said that Naval State University will probably be the first university in the Region 8 to be ISO Certified, if the recommendation will be approved.</p>

						<!-- read more -->
						<div id="view" class="w3-hide">
						<h5><strong>Purpose</strong></h5>
						<p class="justify"><span class="margin-3"></span>“When the school is certified as a higher education institution, it means that they are after for quality assurance and [after for] the system and processes in order to meet the higher education quality standard” Mr. Carlito Cabas said during an interview.</p>
						<p class="justify"><span class="margin-3"></span>He further said that since the vision of NSU is to become globally competitive state university, the school strongly believes that the ISO Certification is the key to achieve this goal. The primary purpose of the school is to excel in the whole world in terms of academic and skills and produce world class talents that will promote our school in different aspects.</p>
						
						<h5><strong>Process</strong></h5>
						<p class="justify"><span class="margin-3"></span>In order for the school to be ISO Certified, it needs to have series of observation, audit and accreditation. An observation was made by the ISO Auditors in order to determine if all the programs offered by the university are eligible to apply for the ISO 9001:2015 Certificate.</p>
						<p class=" justify"><span class="margin-3"></span>In addition, some areas accredited by the auditors include the different departments, offices, classroom observations, percentage of board passers, topnotchers and quality management system of the school.</p>
						<p class="justify"><span class="margin-3"></span>Moreover, prior to the DNV audit last July 19-20, College of Maritime Education programs, which were already ISO 9001:2002 certified but will expire on September, was audited last June 26 to transition from the old version to the latest ISO 9001:2015.</p>

						<h5><strong>Result</strong></h5>
						<p class="justify"><span class="margin-3"></span>After series of evaluation, Miss Cerrafon, declared that there were no identified non-conformities, four observations, four opportunities for improvement, and nine noteworthy efforts on the tally of results during the audit.</p>
						<p class="justify"><span class="margin-3"></span>According to Mr. Cabas, the current administration funded the amount of more or less Php500, 000 in order for the observation to be realized. The papers that were used during the observation of the auditors were submitted to DNV-GL in Singapore and the university will wait for about two to three months for the certificate to be received.</p>
						<p class="justify"><span class="margin-3"></span>Moreover, ISO Certification is a seal of approval from a third body that a company runs to one of the internationally recognized ISO management systems. The certification can be used to tender for business as a proof of company’s credibility but also to install confidence in the potential client that you will keep your promise.</p>
						<p class="justify"><span class="margin-3"></span>The said endeavor is a component of the “Yes to: Commencing Application for Quality Assurance,” under the NSU is YOU banner program of the administration.</p>
						</div>
						<!-- ./read more -->
						<br>
						<div class="like-comment-readmore">
							<br>			
							<a onclick="myAccFunc('view')" class="read-more" name="viewbtn" style="margin-right: 2em;" id="readMore"></a>
						</div>
						<br>
					</div> 
				</div>
				<br>
			</section>  <!-- /#sidebar-entertainment-post -->


			<section id="ccr-sidebar-add-place">
				<div>
					<img src="img/pic_release/new-cover.jpg">
				</div>
				<br>
				<div>
					<img src="img/pic_release/capture.png">
				</div>
				<br>
			</section> <!-- /#ccr-sidebar-add-place -->


		</aside><!-- /.col-md-8 / #ccr-left-section -->